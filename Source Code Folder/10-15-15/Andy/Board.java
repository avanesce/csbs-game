import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

import javax.imageio.*;
import javax.imageio.stream.ImageInputStream;
public class Board {
	//Dynamic Panel Objects
	JLabel player1, player2, player3;
	JButton b2 = new JButton("Move");
	static JList roomList;
	JScrollPane roomListPane = new JScrollPane(roomList);
	static String listHolder;
	ActionListener buttonListener;
	
	void createAndDrawGUI() throws IOException {
		//Master frame
		JFrame masterFrame = new JFrame("CSBS Challenge Game");
		/******************TOP PANEL*********************/
		//Image Frame: Import Image and add Scroll Pane
		InputStream input = new FileInputStream("C:/Users/Ireum/workspace/343 CSBS/src/BoardImage.png");
		ImageInputStream imageInput = ImageIO.createImageInputStream(input);
		JLabel imageLabel = new JLabel(new ImageIcon(ImageIO.read(imageInput)));
		imageLabel.add(player1);
		imageLabel.add(player2);
		imageLabel.add(player3);
		
		JScrollPane topLabel = new JScrollPane(imageLabel,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		  
		/*****************BOTTOM PANEL*******************/
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(null);
		//DYNAMIC OBJECTS ARE CREATED IN GAMESTATE
		//Buttons
		JButton b1 = new JButton("Draw Card");
		//JButton b2 = new JButton("Move");
		JButton b3 = new JButton("Play Card");
		
		//Bottom Text Area: Game Log
		JTextArea gameLogText = new JTextArea();
		gameLogText = new JTextArea(1,1); 
		JScrollPane gameLogPane = new JScrollPane(gameLogText, 
			ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, 
			ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER); 
		
		//Top Right Text Area
		DefaultListModel model2 = new DefaultListModel();
		JList scoreboardList = new JList(model2);
		JScrollPane scoreboardPane = new JScrollPane(scoreboardList);
		
		//Add buttons buttons and text fields to bottom panel
		bottomPanel.add(scoreboardPane);
		bottomPanel.add(gameLogPane); 
		bottomPanel.add(roomListPane);
		bottomPanel.add(b1);
		bottomPanel.add(b2);
		bottomPanel.add(b3);
		
		//Bottom panel configurations
		Insets insets = bottomPanel.getInsets();
		
		Dimension size = b1.getPreferredSize();
		b1.setBounds(0 + insets.left, 0 + insets.top,
			size.width + 60, size.height);
		
		size = b2.getPreferredSize();
		b2.setBounds(0 + insets.left, 25 + insets.top,
			size.width + 90 , size.height);
		
		size = b3.getPreferredSize();
		b3.setBounds(0 + insets.left, 50 + insets.top,
			size.width + 66 , size.height );
		
		size = roomListPane.getPreferredSize();
		roomListPane.setBounds(0 + insets.left, 75 + insets.top,
			size.width + 94, size.height );
		
		size = gameLogPane.getPreferredSize();
		gameLogPane.setBounds(500 + insets.left, 130 + insets.top,
			size.width +600 , size.height);
		
		size = scoreboardPane.getPreferredSize();
		scoreboardPane.setBounds(500 + insets.left, 10 + insets.top,
			size.width +600 , size.height);
		
		//Creates the MOVE BUTTON LISTENER 
	    roomList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    roomList.addListSelectionListener(new ListSelectionListener() {
	    	public void valueChanged(ListSelectionEvent event) {
	    		listHolder = (String) roomList.getSelectedValue();
	    	}
	    }); 
	    //MOVE BUTTON LISTENER
	    /*b2.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
		    	Room r = new Room();
		    	redrawPlayer(r, 1);
		    	
		    }
		});*/ 
	
		
		//Add top and bottom panels to SplitPane, Add SplitPane to MasterFrame
		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, topLabel, bottomPanel);
		splitPane.setDividerLocation(500);
		masterFrame.setContentPane(splitPane);
		masterFrame.setVisible(true);
		masterFrame.setResizable(true);
		masterFrame.setSize(1670,2000);
		
		masterFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	/**Button Function Control**/
	public void updateMoveButton(Room room, Room[] roomArray, int i){
	    //MOVE BUTTON LISTENER
		b2.removeActionListener(buttonListener);
		buttonListener = createActionListener(room, roomArray, i);
	    b2.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
		    	redrawPlayer(room, i);
		    	redrawMoveList(room, roomArray);
		    }
		});
	}
	
	public ActionListener createActionListener(Room room, Room[] roomArray, int i){
		return new ActionListener(){
		    public void actionPerformed(ActionEvent e){
		    	redrawPlayer(room, i);
		    	redrawMoveList(room, roomArray);
		    }
		};
	}
	
	/**Move List Control**/
	public void createMoveList(Room room, Room[] roomArray){
		DefaultListModel<String> model = new DefaultListModel<>();
		for(int i = 0; i < room.adjacentRooms.length; i++){
			model.addElement(roomArray[room.adjacentRooms[i]].getRoomName());
		}
		roomList = new JList(model);
		roomListPane = new JScrollPane(roomList);
	}
	
	public void redrawMoveList(Room room, Room[] roomArray){
		DefaultListModel<String> model = new DefaultListModel<>();
		for(int i = 0; i < room.adjacentRooms.length; i++){
			model.addElement(roomArray[room.adjacentRooms[i]].getRoomName());
			System.out.println(roomArray[room.adjacentRooms[i]].getRoomName());
		}
		//Room Names copy into model"list" but list doesn't not properly repaint on GUI
		roomList.setModel(model);
	}
	
	/**Player Label Control**/
	public void createPlayerLabel(Player player, int i){
		if(i==0){
			player1 = new JLabel(player.getPlayerName());
			playerLabelSetup(player1, i);
		}
		else if(i==1){
			player2 = new JLabel(player.getPlayerName());
			playerLabelSetup(player2, i);
		}
		else{
			player3 = new JLabel(player.getPlayerName());
			playerLabelSetup(player3, i);
		}
	}
	
	public void playerLabelSetup(JLabel label, int i){
		label.setFont(player1.getFont().deriveFont(Font.BOLD, 34));
		label.setForeground(Color.RED);
		label.setSize(150, 30);
	}
	
	public void redrawPlayer(Room room, int i){
		int posX = room.getPosX();	
		int posY = room.getPosY() + (i * 40);
		if(i==0)
			player1.setLocation(posX, posY);
		else if(i==1)
			player2.setLocation(posX, posY);
		else
			player3.setLocation(posX, posY);
	}
}
