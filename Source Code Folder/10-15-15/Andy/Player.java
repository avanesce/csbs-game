/* CLASS PLAYER
 * Function: Individual player object that contains
 * 	all values that determine a player's validity
 *  in movement and card play.
 */
public class Player {
	final int DEFAULT_MOVE = 3;
	final int DEFAULT_ROOM = 1;
	
	String playerName;
	int currentRoom;
	int learnPt;
	int craftPt;
	int integPt;
	int qlityPt;
	int skillChips;
	int moveCount = 3;
	Card[] hand;
	
	//Default Constructor
	public Player(){}
	//Constructor
	public Player(String newName){
		playerName = newName;
	}
	/************************************************/
	/***********Standard GET/SET FUCNTIONS***********/
	/************************************************/
	String getPlayerName(){
		return playerName;
	}
	
	void setPlayerName(String newPlayerName){
		playerName = newPlayerName;
	}
	
	int getCurrentRoom(){
		return currentRoom;
	}
	
	void setCurrentRoom(int newCurrentRoom){
		currentRoom = newCurrentRoom;
	}
	
	int getLearnPt(){
		return learnPt;
	}
	
	int getCraftPt(){
		return craftPt;
	}
	
	int getQlityPt(){
		return qlityPt;
	}
	
	//Function updates all points at once
	void setAllPoints(int newLearnPt, int newCraftPt, int newQlityPt){
		learnPt = newLearnPt;
		craftPt = newCraftPt;
		qlityPt = newQlityPt;
	}
	
	int getSkillChips(){
		return skillChips;
	}
	
	void setSkillChips(int newSkillChips){
		skillChips = newSkillChips;
	}
	
	/************************************************/
	/************************************************/
	
	void resetMoveCount(){
		moveCount = DEFAULT_MOVE;
	}
	
	void decrMoveCount(){
		moveCount--;
	}
	
	void addCard(Card newCard){
		//hand.add(newCard);
	}
	
	void discardCard(int cardInHand){
		//discardPile.add(hand[int]);
		//hand[int] = null;
	}
	
}
