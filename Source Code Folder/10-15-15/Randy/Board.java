import javax.swing.*;
import java.awt.Dimension;
import java.awt.Insets;
import java.io.*;

import javax.imageio.*;
import javax.imageio.stream.ImageInputStream;
public class Board {
	JLabel player1 = new JLabel("Matt");
	void createAndDrawGUI() throws IOException {
		//Master frame
		JFrame masterFrame = new JFrame("CSBS Background Frame");
		//Image Frame: Import Image and add Scroll Pane
		InputStream input = new FileInputStream("C:/Users/Ireum/workspace/343 CSBS/src/BoardImage.png");
		ImageInputStream imageInput = ImageIO.createImageInputStream(input);
		JLabel JL = new JLabel(new ImageIcon(ImageIO.read(imageInput)));
		JScrollPane topLabel = new JScrollPane(JL,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		  
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(null);
		//Buttons
		JButton b1 = new JButton("Draw Card");
		JButton b2 = new JButton("Move");
		JButton b3 = new JButton("Play Card");
		//Scroll Pane
		DefaultListModel model = new DefaultListModel();
		JList roomList = new JList(model);
		JScrollPane roomListPane = new JScrollPane(roomList);
		//Bottom Text Area: Game Log
		JTextArea gameLogText = new JTextArea();
		gameLogText = new JTextArea(1,1); 
		JScrollPane gameLogPane = new JScrollPane(gameLogText, 
			ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, 
			ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER); 
		//Top Right Text Area
		DefaultListModel model2 = new DefaultListModel();
		JList scoreboardList = new JList(model2);
		JScrollPane scoreboardPane = new JScrollPane(scoreboardList);
			
		
		/*
		JLabel player1 = new JLabel("LABEL 1");
		player1.setLocation(50,50);
		player1.setSize(86, 14);
		topLabel.add(player1);
		*/
		
		//Add buttons buttons and text fields to bottom panel
		bottomPanel.add(scoreboardPane);
		bottomPanel.add(gameLogPane); 
		bottomPanel.add(roomListPane);
		bottomPanel.add(b1);
		bottomPanel.add(b2);
		bottomPanel.add(b3);
		
		//Bottom panel configurations
		Insets insets = bottomPanel.getInsets();
		
		Dimension size = b1.getPreferredSize();
		b1.setBounds(0 + insets.left, 0 + insets.top,
			size.width, size.height);
		
		size = b2.getPreferredSize();
		b2.setBounds(0 + insets.left, 25 + insets.top,
			size.width + 30 , size.height);
		
		size = b3.getPreferredSize();
		b3.setBounds(0 + insets.left, 50 + insets.top,
			size.width + 6 , size.height );
		
		size = roomListPane.getPreferredSize();
		roomListPane.setBounds(0 + insets.left, 75 + insets.top,
			size.width , size.height );
		
		size = roomListPane.getPreferredSize();
		gameLogPane.setBounds(500 + insets.left, 130 + insets.top,
			size.width +600 , size.height -65);
		
		size = roomListPane.getPreferredSize();
		scoreboardPane.setBounds(500 + insets.left, 10 + insets.top,
			size.width +600 , size.height -20);
		
		//Add top and bottom panels to SplitPane, Add SplitPane to MasterFrame
		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, topLabel, bottomPanel);
		splitPane.setDividerLocation(500);
		masterFrame.setContentPane(splitPane);
		masterFrame.setVisible(true);
		masterFrame.setResizable(true);
		masterFrame.setSize(1670,2000);
		
		masterFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	 
	public void redrawPlayer(Room room, Player player, int i){
		int posX = room.getPosX();	
		int posY = room.getPosY() + (i * 40);
		player1.setLocation(posX, posY);
	}

}
