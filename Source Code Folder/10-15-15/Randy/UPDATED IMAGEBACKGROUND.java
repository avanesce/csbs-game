import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.io.*;

import javax.imageio.*;
import javax.imageio.stream.ImageInputStream;
public class ImageBackground {
	
	public static void main(String[] args) throws IOException {
		JFrame F = new JFrame("CSBS Background Frame");
		InputStream input = new FileInputStream("C:/Users/Randy/Desktop/CSULBMap3.png");
	   ImageInputStream imageInput = ImageIO.createImageInputStream(input);
	   JPanel imagePanel = new JPanel();
	   //Creates a label to store the CSULBMap.png image
	   JLabel background = new JLabel(new ImageIcon(ImageIO.read(imageInput)));
	   background.setLayout(null);
	   imagePanel.add(background);
	   
	   JLabel label = new JLabel("Player 1");
	   label.setFont(label.getFont().deriveFont(Font.BOLD, 14));
	   label.setForeground(Color.RED);
	   label.setLocation(430,300);
	  label.setSize(500,100);
	   background.add(label);
	   imagePanel.add(background);
	   
	   
	  
	   JScrollPane topLabelCSULBMAP = new JScrollPane(imagePanel,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
     
      JPanel leftPanel = new JPanel();
      leftPanel.setLayout(null);

      JButton b1 = new JButton("Draw Card");
      JButton b2 = new JButton("Move");
      JButton b3 = new JButton("Play Card");
      
      DefaultListModel model = new DefaultListModel();
	   JList list = new JList(model);
	   JScrollPane pane = new JScrollPane(list);
	   
	   JTextArea commentTextArea = new JTextArea();
	   commentTextArea = new JTextArea(1,1); 
		JScrollPane textArea1 = new JScrollPane(commentTextArea, 
															ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, 
															ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER); 
		
		DefaultListModel model2 = new DefaultListModel();
	   JList list2 = new JList(model);
	   JScrollPane commentTextArea2 = new JScrollPane(list2);
		
		leftPanel.add(commentTextArea2);
		leftPanel.add(textArea1); 
	   leftPanel.add(pane);
      leftPanel.add(b1);
      leftPanel.add(b2);
      leftPanel.add(b3);
      
      Insets insets = leftPanel.getInsets();
      
      Dimension size = b1.getPreferredSize();
      b1.setBounds(0 + insets.left, 0 + insets.top,
                   size.width, size.height);
      
      size = b2.getPreferredSize();
      b2.setBounds(0 + insets.left, 25 + insets.top,
                   size.width , size.height);
      
      size = b3.getPreferredSize();
      b3.setBounds(0 + insets.left, 50 + insets.top,
                   size.width , size.height );
      
      size = pane.getPreferredSize();
      pane.setBounds(0 + insets.left, 75 + insets.top,
                   size.width , size.height );
      
      size = pane.getPreferredSize();
      textArea1.setBounds(500 + insets.left, 130 + insets.top,
                   size.width +600 , size.height -65);
      
      size = pane.getPreferredSize();
      commentTextArea2.setBounds(500 + insets.left, 10 + insets.top,
                   size.width +600 , size.height -20);
      
                   
     
      JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, topLabelCSULBMAP, leftPanel);
      splitPane.setDividerLocation(500);
      F.setContentPane(splitPane);
		F.setVisible(true);
		F.setResizable(true);
		F.setSize(1670,2000);
		
		
	}

}
