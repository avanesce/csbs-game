/* CLASS CARD
 * Function: Determine characteristics of each card.
 *  Cards are used in play to simulate activities and
 *  to advance the game state.
 */

/*
 * TO BE IMPLEMENTED IN UPCOMING ITERATIONS
 */
public class Card {
	String cardName;
	String description;
	int roomReq;
	int yearReq;
	int pointsReq;
	int qpPenalty;
}
