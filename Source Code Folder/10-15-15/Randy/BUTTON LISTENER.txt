b2.addActionListener(new ActionListener() {
      	 
         public void actionPerformed(ActionEvent e)
         {
         	if(listHolder.contains("South Hall")){
         		label.setLocation(830,1170);
         		//tempRoom =r.getRoomFromSTRING(listHolder);
         		tempRoom = new Room("South Hall", 830, 1170, new int[]{14,18,19,17,20,10});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("George Allen Field")) {
         		label.setLocation(20,20);
         		//tempRoom =r.getRoomFromSTRING(listHolder);
         		tempRoom = new Room("George Allen Field", 20, 20, new int[]{1,5,3,4});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Japanese Garden")) {
         		label.setLocation(430,20);
         		//tempRoom =r.getRoomFromSTRING(listHolder);
         		tempRoom = new Room("Japanese Garden", 430, 20, new int[]{0,3,2});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Student Parking")) {
         		label.setLocation(920,20);
         		//tempRoom =r.getRoomFromSTRING(listHolder);
         		tempRoom = new Room("Student Parking", 920, 20, new int[]{1,3,5,6});
         		
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("The Pyramid")) {
         		label.setLocation(430,300);
         		//tempRoom =r.getRoomFromSTRING(listHolder);
         		tempRoom = new Room("The Pyramid", 430, 300, new int[]{0,1,2,5});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("West Walkway")) {
         		label.setLocation(20, 670);
         		//tempRoom =r.getRoomFromSTRING(listHolder.toString());
         		tempRoom = new Room("West Walkway", 20, 670, new int[]{0,5,12,7});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Health Center")) {
         		label.setLocation(450,540);
         		//tempRoom =r.getRoomFromSTRING(listHolder);
         		tempRoom = new Room("Health Center", 450, 540, new int[]{0,3,4,2,6});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Forbidden Parking")) {
         		label.setLocation(1020,520);
         		//tempRoom =r.getRoomFromSTRING(listHolder);
         		tempRoom = new Room("Forbidden Parking", 1020, 520, new int[]{2,5,10});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Library")) {
         		label.setLocation(20,1540);
         		//tempRoom =r.getRoomFromSTRING(listHolder);
         		tempRoom = new Room("Library", 20, 1540, new int[]{4,8});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Union")) {
         		label.setLocation(480,1640);
         		//tempRoom =r.getRoomFromSTRING(listHolder);
         		tempRoom =  new Room("Union", 480, 1640, new int[]{7,9,16});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Bratwurst Hall")) {
         		label.setLocation(1050,1660);
         		//tempRoom =r.getRoomFromSTRING(listHolder);
         		tempRoom = new Room("Bratwurst Hall", 1050, 1660, new int[]{7,10});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("East Walkway")) {
         		label.setLocation(1480,960);
         		//tempRoom =r.getRoomFromSTRING(listHolder);
         		tempRoom = new Room("East Walkway", 1480, 960, new int[]{9,6,15});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Lab")) {
         		label.setLocation(180,890);
         		//tempRoom =r.getRoomFromSTRING(listHolder);
         		tempRoom = new Room("Lab", 180, 890, new int[]{12});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("North Hall")) {
         		label.setLocation(180,1170); 
         		//tempRoom =r.getRoomFromSTRING(listHolder);
         		tempRoom = new Room("North Hall", 180, 1170, new int[]{4,11,14,13,16,15});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3); 
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Room of Retirement")) {
         		label.setLocation(180,1360);
         		//tempRoom =r.getRoomFromSTRING(listHolder);
         		tempRoom =new Room("Room of Retirement", 180, 1360, new int[]{12});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3); 
         	   pane.repaint();
         	}
         	else if(listHolder.contains("302")) {
         		label.setLocation(620,890);
         		//tempRoom =r.getRoomFromSTRING(listHolder);
         		tempRoom =new Room("302", 620, 890, new int[]{12,15});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3); 
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Elevators")) {
         		label.setLocation(620,1360);
         		//tempRoom =r.getRoomFromSTRING(listHolder);
         		tempRoom = new Room("Elevators", 620, 1360, new int[]{12,8});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("308")) {
         		label.setLocation(830,1360);
         		//tempRoom =r.getRoomFromSTRING(listHolder);
         		tempRoom =new Room("308", 830, 1360, new int[]{15});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3); 
         	   pane.repaint();
         	}
         	else if(listHolder.contains("EAT")) {
         		label.setLocation(1050,890);
         		//tempRoom =r.getRoomFromSTRING(listHolder);
         		tempRoom =new Room("EAT", 1050, 890, new int[]{15});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3); 
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Conference")) {
         		label.setLocation(1260,890);
         		//tempRoom =r.getRoomFromSTRING(listHolder);
         		tempRoom =new Room("Conference", 1260, 890, new int[]{15});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3); 
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Noisy Room")) {
         		label.setLocation(1230,1360);
         		//tempRoom =r.getRoomFromSTRING(listHolder);
         		tempRoom =new Room("Noisy Room", 1230, 1360, new int[]{15});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3); 
         	   pane.repaint();
         	}
 
         }
     });      