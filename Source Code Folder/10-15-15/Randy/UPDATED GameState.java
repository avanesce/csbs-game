/* CLASS GAMESTATE
 * Function: Runs main method.
 *  Keeps track of global game values:
 *  	RoomList
 *  	Deck
 *  	Discard Pile
 *  	Current Game Year
 *  	Total Quality Points of all players
 */
import java.io.IOException;
import java.util.*;

public class GameState {
	final static Player[] playerList = populatePlayers();
	final static Room[] roomList = populateRooms();
	//Used for implementation later
	Card[] deck;
	Card[] discardPile;
	int year = 1;
	int totalQP = 0;
	
	//Main Functions
	public static void main(String[] args) throws IOException{
		Board board = new Board();
		board.createAndDrawGUI();
		
		int i = 1;
		board.redrawPlayer(roomList[playerList[i].getCurrentRoom()], playerList[i], i);
				
	}
	/************************************************/
	/**************GAME SETUP FUNCTIONS**************/
	/************************************************/
	/* FUCNTION POPULATEPLAYERS
	 * Arguments: None
	 * Returns: Array of players [0-2]
	 * Randomly assigns names to players.
	 * Player 1 [0] is always human and plays first.
	 */
	static Player[] populatePlayers(){
		Random r = new Random();
		String[] names = {"Deez Nuts", "Hugh E. Rection", "Mike"};
		int n = r.nextInt(3);
		Player[] players = new Player[3];
		for(int i = 0; i < 3; i++){
			players[i] = new Player(names[n]);
			players[i].setCurrentRoom(0);
			n++;
			if(n == 3)
				n = 0;
		}
		return players;
	}
	
	/* FUCNTION POPULATEPLAYERS
	 * Arguments: None
	 * Returns: Array of rooms [0-19]
	 * Creates all game rooms and inserts into an array.
	 * Room values are all hard-coded.
	 */
	static Room[] populateRooms(){
		 Room[] rooms = {
    	 new Room("George Allen Field", 20, 20, new int[]{1,5,3,4}), //0
	 	 new Room("Japanese Garden", 430, 20, new int[]{0,3,2}), //1
		 new Room("Student Parking", 920, 20, new int[]{1,3,5,6}), //2
		 new Room("The Pyramid", 430, 300, new int[]{0,1,2,5}), //3
		 new Room("West Walkway", 20, 670, new int[]{0,5,12,7}), //4
		 new Room("Health Center", 450, 540, new int[]{0,3,4,2,6}), //5
		 new Room("Forbidden Parking", 1020, 520, new int[]{2,5,10}), //6
		 new Room("Library", 20, 1540, new int[]{4,8}), //7
		 new Room("Union", 480, 1640, new int[]{7,9,16}), //8
		 new Room("Bratwurst Hall", 1050, 1660, new int[]{7,10}), //9
		 new Room("East Walkway", 1480, 960, new int[]{9,6,15}), //10
		 new Room("Lab", 180, 890, new int[]{12}), //11
		 new Room("North Hall", 180, 1170, new int[]{4,11,14,13,16,15}), //12
		 new Room("Room of Retirement", 180, 1360, new int[]{12}), //13 
		 new Room("302", 620, 890, new int[]{12,15}), //14
		 new Room("South Hall", 830, 1170, new int[]{14,18,19,17,20,10}), //15
		 new Room("Elevators", 620, 1360, new int[]{12,8}), //16
		 new Room("308", 830, 1360, new int[]{15}), //17
		 new Room("EAT", 1050, 890, new int[]{15}), //18
		 new Room("Conference", 1260, 890, new int[]{15}), //19
		 new Room("Noisy Room", 1230, 1360, new int[]{15}) //20
		};
		return rooms;
	}
}
