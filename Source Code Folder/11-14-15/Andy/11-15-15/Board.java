/* CLASS BOARD
 * Function: Creates the game's visual interface
 * Provides objects for
 * 	Game Map
 * 	
 */

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

import javax.imageio.*;
import javax.imageio.stream.ImageInputStream;
public class Board {
	//Master Window
	private static JFrame masterFrame = new JFrame("CSBS Challenge Game");
	private static JScrollPane topPanel;
	private static JPanel bottomPanel = new JPanel();
	//Top Board Components
	static JLabel boardImage, player1, player2, player3;
	//Bottom Interface Components
		//Buttons
	JButton drawCardButton = new JButton("Draw Card");
	JButton moveButton = new JButton("Move");
	JButton playCard3utton = new JButton("Play Card");
	ActionListener buttonListener;
	
		//Room List
	JList roomList;
	String listHolder;
	JScrollPane roomListPane = new JScrollPane(roomList);
		//Card Image
	
		//Score Board
	DefaultListModel model2 = new DefaultListModel();
	JList scoreboardList; 
	JScrollPane scoreboardPanel;
		//Current Play
	JTextArea currentPlayText = new JTextArea();
	JScrollPane currentPlayPanel;
	
	
	/***************OVERALL SETUP FUNCTION***************/
	void createAndDrawGUI() throws IOException {
		/***************TOP PANEL SETUP***************/
		createBoard();
		/***************BOTTOM PANEL SETUP***************/
		createUI();
		//Add Top/Bot panel to master frame, sets settings
		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, topPanel, bottomPanel);
		splitPane.setDividerLocation(500);
		masterFrame.setContentPane(splitPane);
		masterFrame.setVisible(true);
		masterFrame.setResizable(true);
		masterFrame.setSize(1670,2000);
		masterFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	
	
	/***************COMPONENT SETUP***************/
	//Creates top half of window
	void createBoard() throws IOException{
		InputStream input = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/BoardImage.png");
		ImageInputStream imageInput = ImageIO.createImageInputStream(input);
	   boardImage = new JLabel(new ImageIcon(ImageIO.read(imageInput)));
		boardImage.add(player1);
		boardImage.add(player2);
		boardImage.add(player3);
		topPanel = new JScrollPane(boardImage,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	}
	
	//Creates bottom half of window
	void createUI(){
		currentPlayText = new JTextArea(1,1);
		currentPlayPanel = new JScrollPane(currentPlayText, 
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, 
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scoreboardList = new JList(model2);
		scoreboardPanel = new JScrollPane(scoreboardList);
		
		bottomPanel.setLayout(null);
		bottomPanel.add(scoreboardPanel);
		bottomPanel.add(currentPlayPanel); 
		bottomPanel.add(roomListPane);
		bottomPanel.add(drawCardButton);
		bottomPanel.add(moveButton);
		bottomPanel.add(playCard3utton);
		//Bottom panel configurations
		Insets insets = bottomPanel.getInsets();
		
		Dimension size = drawCardButton.getPreferredSize();
		drawCardButton.setBounds(0 + insets.left, 0 + insets.top,
			size.width + 60, size.height);
		
		size = moveButton.getPreferredSize();
		moveButton.setBounds(0 + insets.left, 25 + insets.top,
			size.width + 90 , size.height);
		
		size = playCard3utton.getPreferredSize();
		playCard3utton.setBounds(0 + insets.left, 50 + insets.top,
			size.width + 66 , size.height );
		
		size = roomListPane.getPreferredSize();
		roomListPane.setBounds(0 + insets.left, 75 + insets.top,
			size.width + 94, size.height );
		
		size = currentPlayPanel.getPreferredSize();
		currentPlayPanel.setBounds(500 + insets.left, 130 + insets.top,
			size.width +600 , size.height);
		
		size = scoreboardPanel.getPreferredSize();
		scoreboardPanel.setBounds(500 + insets.left, 10 + insets.top,
			size.width +600 , size.height);
		
		moveButtonSetup();
	}
	
	
	
	/***************COMPONENT ACTIONS***************/
	/* Function creates the button listener for moveButton
	 * Updates adjacent room list and player location when activated
	 */
	void moveButtonSetup(){
		roomList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		roomList.addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent event){
				listHolder = (String) roomList.getSelectedValue();
			}
		});
		
		moveButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				//String selectedRoom = listHolder.contains("South Hall");
				Room tempRoom = null;
				int j = 0;
				for(int i = 0; i < 21; i++){
					if(GameState.roomList[i].getRoomName().equals(listHolder)){
						tempRoom = GameState.roomList[i];
						j = i;
					}
				}
				switch(GameState.currentPlayer){
				case 0:
					redrawPlayer(tempRoom, GameState.currentPlayer);
					break;
				case 1:
					redrawPlayer(tempRoom, GameState.currentPlayer);
					break;
				case 2:	
					redrawPlayer(tempRoom, GameState.currentPlayer);
					break;
				}
				GameState.playerList[GameState.currentPlayer].setCurrentRoom(j);
				
				redrawMoveList(tempRoom, GameState.roomList);
				roomList.repaint();
				Player tempPlayer =  GameState.playerList[GameState.currentPlayer];
				//MOVE THIS TO CURRENT PLAY TEXT AREA
				System.out.println("Player " + tempPlayer.getPlayerName() +
										 " has moved to " + GameState.roomList[tempPlayer.getCurrentRoom()].getRoomName());
				GameState.currentPlayer++; //TESTING PURPOSES ONLY, REMOVE LATER
			}
		});
	}
	
	//Function creates the INITIAL list of adjacent rooms displayed
	public void createMoveList(Room room, Room[] roomArray){
		DefaultListModel<String> model = new DefaultListModel<>();
		for(int i = 0; i < room.adjacentRooms.length; i++){
			model.addElement(roomArray[room.adjacentRooms[i]].getRoomName());
		}
		roomList = new JList(model);
		roomListPane = new JScrollPane(roomList);
	}
	
	//Function UPDATES the list of adjacent rooms displayed
	public void redrawMoveList(Room room, Room[] roomArray){
		DefaultListModel<String> model = new DefaultListModel<>();
		for(int i = 0; i < room.adjacentRooms.length; i++){
			model.addElement(roomArray[room.adjacentRooms[i]].getRoomName());
		}
		roomList.setModel(model);
	}
	

	//Function creates the INITIAL set of player tokens
	public void createPlayerLabel(String playerName, int i){
		if(i==0){
			player1 = new JLabel(playerName);
			playerTokenSetup(player1, i);
		}
		else if(i==1){
			player2 = new JLabel(playerName);
			playerTokenSetup(player2, i);
		}
		else{
			player3 = new JLabel(playerName);
			playerTokenSetup(player3, i);
		}
	}
	
	//Function changes the INITIAL settings of player tokens
	public void playerTokenSetup(JLabel label, int i){
		label.setFont(player1.getFont().deriveFont(Font.BOLD, 34));
		label.setForeground(Color.RED);
		label.setSize(150, 30);
	}
	
	//Function UPDATES the player tokens
	public static void redrawPlayer(Room room, int i){
		int posX = room.getPosX();	
		int posY = room.getPosY() + (i * 40);
		if(i == 0)
			player1.setLocation(posX, posY);
		else if(i == 1)
			player2.setLocation(posX, posY);
		else
			player3.setLocation(posX, posY);
	}
}
