import java.io.FileNotFoundException;
import java.io.IOException;

import javax.imageio.stream.ImageInputStream;
import javax.swing.JLabel;

/* CLASS CARD
 * Function: Determine characteristics of each card.
 *  Cards are used in play to simulate activities and
 *  to advance the game state.
 */

/*
 * TO BE IMPLEMENTED IN UPCOMING ITERATIONS
 */
public abstract class Card {
	String cardName;
	String description;
	int roomReq;
	int yearReq;
	int pointsReq;
	int qpPenalty;
	
	//Each Card has a different method that returns values to the caller
	//The values are an int array of 3 slots that contains points
	//The values are Learning, Crafting, and Integrity
	abstract int[] play();
	abstract ImageInputStream displayPIcture();
}
