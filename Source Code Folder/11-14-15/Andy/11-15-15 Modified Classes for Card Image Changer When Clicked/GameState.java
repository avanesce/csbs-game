/* CLASS GAMESTATE
 * Function: Runs main method.
 *  Keeps track of global game values:
 *  	RoomList
 *  	Deck
 *  	Discard Pile
 *  	Current Game Year
 *  	Total Quality Points of all players
 */
import java.io.IOException;
import java.util.*;

public class GameState {
	public static Player[] playerList = new Player[3];
	public static Room[] roomList;
	public static int currentPlayer = 0;
	public static Room currentPlayerRoom;
	//Used for implementation later
	static List<Card> deck = new ArrayList<Card>();
	static ArrayList discardPile = new ArrayList();
	int year = 1;
	int totalQP = 0; 
	
	//Main Functions
	public static void main(String[] args) throws IOException{
		/**************INITIAL GAME SETUP***************/
		//Create game elements
		initialSetup();
		//currentPlayerRoom = roomList[playerList[playerTurn].getCurrentRoom()];
		//board.updateMoveButton(currentPlayerRoom, roomList, playerTurn);
		
		/**************TESTING SECTION***************/

		/**************MAIN GAME LOOP***************/
		
		//GAME LOOP - until a player reaches 100 QP
		while(playerList[0].getQlityPt()<100 ||
		      playerList[1].getQlityPt()<100 ||
		      playerList[2].getQlityPt()<100){
			
			//Player Turn
			if(currentPlayer == 0){
				playerTurn(playerList[currentPlayer]);
			}
			
			//AI Turn
			else{
				aiTurn(playerList[currentPlayer]);
			}
			//Check round reset
			if(currentPlayer==3)
				currentPlayer = 0;
		}
		
		if(playerList[0].getQlityPt()<100){
			//output to CurrentPlayPanel player 1 has won
		}
		else if(playerList[0].getQlityPt()<100){
			//output to CurrentPlayPanel player 2 has won
		}
		else{
			//output to CurrentPlayPanel player 3 has won
		}
		
	}
	/**************GAME PLAY FUNCTIONS***************/
	/* FUCNTION playerTurn
	 * Arguments: Player
	 * Returns: None
	 * Function allows players to move
	 * 	Selects an appropriate card if possible
	 * 	Assigns points where needed
	 * 	Ends turn
	 */
	static void playerTurn(Player player){
		player.drawCard(deck);
		player.resetMoveCount();
		while(player.getMoveCount() > 0){
			//listen for move
			player.decrMoveCount();
		}
		//playCard();
		currentPlayer++;
	}
	
	/* FUCNTION AITURN
	 * Arguments: Player
	 * Returns: None
	 * Automatic algorithm for computer play
	 * Function randomly moves AI player
	 * 	Selects an appropriate card if possible
	 * 	Assigns points where needed
	 * 	Ends turn
	 */
	static void aiTurn(Player player){
		player.resetMoveCount();
		while(player.getMoveCount() > 0){
			//listen for move
			player.decrMoveCount();
		}
		//playCardAI();
		currentPlayer++;
	}
	
	/*
	 * 
	 */
	static void playCardAI(){
		
	}
	/**************GAME SETUP FUNCTIONS**************/	
	/* Function INITIAL SETUP
	 * Creates the game window (board+ui)
	 * Creates the players and rooms
	 * Loads player labels onto the board
	 */
	
	static void initialSetup() throws IOException{
		Board board = new Board();
		createPlayers();
		createRooms();	
		//Initial Player Tokens and Window Board setup
		for (int i = 0; i < 3; i++){
			board.createPlayerLabel(playerList[i].getPlayerName(),i);
			playerList[i].setCurrentRoom(17);
			board.redrawPlayer(roomList[playerList[i].getCurrentRoom()], i);
		}
		board.createMoveList(roomList[playerList[currentPlayer].getCurrentRoom()], roomList);
		board.createAndDrawGUI();
	}
	
	/* FUCNTION CREATEPLAYERS
	 * Arguments: None
	 * Returns: Array of players [0-2]
	 * Randomly assigns names to players.
	 * Player 1 [0] is always human and plays first.
	 */
	static void createPlayers(){
		Random r = new Random();
		String[] names = {"Matt", "Kevin", "Bob"};
		int n = r.nextInt(3);
		int[][] defaultPts = {
			{2, 2, 2},
			{3, 1, 2},
			{0, 3, 3}
		};
		for(int i = 0; i < 3; i++){
			playerList[i] = new Player(names[n]);
			playerList[i].setAllPoints(defaultPts[i]);
			n++;
			if(n == 3)
				n = 0;
		}
	}
	
	/* FUCNTION CREATEROOMS
	 * Arguments: None
	 * Returns: Array of rooms [0-19]
	 * Creates all game rooms and inserts into an array.
	 * Room values are all hard-coded.
	 */
	static void createRooms(){
		Room[] rooms = {
		 new Room("George Allen Field", 20, 20, new int[]{1,5,3,4}), //0
	 	 new Room("Japanese Garden", 430, 20, new int[]{0,3,2}), //1
		 new Room("Student Parking", 920, 20, new int[]{1,3,5,6}), //2
		 new Room("The Pyramid", 430, 300, new int[]{0,1,2,5}), //3
		 new Room("West Walkway", 20, 670, new int[]{0,5,12,7}), //4
		 new Room("Rec Center", 450, 540, new int[]{0,3,4,2,6}), //5
		 new Room("Forbidden Parking", 1020, 520, new int[]{2,5,10}), //6
		 new Room("Library", 20, 1540, new int[]{4,8}), //7
		 new Room("Union", 480, 1640, new int[]{7,9,16}), //8
		 new Room("Bratwurst Hall", 1050, 1660, new int[]{7,10}), //9
		 new Room("East Walkway", 1480, 960, new int[]{9,6,15}), //10
		 new Room("Lab", 180, 890, new int[]{12}), //11
		 new Room("North Hall", 180, 1170, new int[]{4,11,14,13,16,15}), //12
		 new Room("Room of Retirement", 180, 1360, new int[]{12}), //13 
		 new Room("302", 620, 890, new int[]{12,15}), //14
		 new Room("South Hall", 830, 1170, new int[]{14,18,19,17,20,10}), //15
		 new Room("Elevators", 620, 1360, new int[]{12,8}), //16
		 new Room("308", 830, 1360, new int[]{15}), //17
		 new Room("EAT", 1050, 890, new int[]{15}), //18
		 new Room("Conference", 1260, 890, new int[]{15}), //19
		 new Room("Noisy Room", 1230, 1360, new int[]{15}) //20
		};
		roomList = rooms;
	}
	
	/* FUNCTION CREATEDECK
	 * Arguments: None
	 * Returns: Arraylist of Cards
	 * Creates the default set of cards and shuffles them
	 */
	static void createDeck(){
		deck.add(new Card1());
		deck.add(new Card2());
		deck.add(new Card3());
		deck.add(new Card4());
		deck.add(new Card5());
		deck.add(new Card6());
		deck.add(new Card7());
		deck.add(new Card8());
		deck.add(new Card9());
		deck.add(new Card10());
		deck.add(new Card11());
		deck.add(new Card12());
		deck.add(new Card13());
		deck.add(new Card14());
		deck.add(new Card15());
		deck.add(new Card16());
		deck.add(new Card17());
		deck.add(new Card18());
		deck.add(new Card19());
		deck.add(new Card20());
		deck.add(new Card21());
		deck.add(new Card22());
		deck.add(new Card23());
		deck.add(new Card24());
		deck.add(new Card25());
		deck.add(new Card26());
		deck.add(new Card27());
		deck.add(new Card28());
		deck.add(new Card29());
		deck.add(new Card30());
		deck.add(new Card31());
		deck.add(new Card32());
		deck.add(new Card33());
		deck.add(new Card34());
		deck.add(new Card35());
		deck.add(new Card36());
		deck.add(new Card37());
		deck.add(new Card38());
		deck.add(new Card39());
		
		
		shuffleDeck();
	}
	
	static void reshuffleDeck(){
		//Shuffle Discard Pile into D
		shuffleDeck();
	}
	
	static void shuffleDeck(){
		long seed = System.nanoTime();
		Collections.shuffle(deck, new Random(seed));
	}
}
