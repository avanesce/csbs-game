import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card36 extends Card{
	String cardName = "Learning Linux"; 
	int roomReq = 11;
	int yearReq = 1;
	int[] reqPoints = {0,2,3,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p){
		if(p.getCurrentRoom() == roomReq
				&& p.getCraft() >= reqPoints[1]
				&& p.getInteg() >= reqPoints[2]){
			//ADDPOINTS
			p.setQual(7);
			//PROMPT FOR CHIP OF CHOICE
			
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 7 Quality Points and a selected chip.\n" );
		}
		else{
			p.setQual(-3);
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 3 Quality Points.\n" );
		}
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card36.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("I ugly");
		}
		return cardInput;
	}

}
