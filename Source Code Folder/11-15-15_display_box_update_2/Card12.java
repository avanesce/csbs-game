import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card12 extends Card{
	String cardName = "Goodbye Professor"; 
	int roomReq = 0;
	int roomReq2 = 18;
	int yearReq = 1;
	int[] reqPoints = {6,6,6,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p){
		if(p.getCurrentRoom()==roomReq || p.getCurrentRoom()==roomReq2){

			//PROMPT LEARNING OR INTGRITY
			
			//LEARNING
			p.setLearn(1);// OR p.setInteg(1); 
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 1 Learning Chip.\n");
			
			//INTEGRITY
			p.setCraft(1); 
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 1 Craft Chip.\n");
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n");
		}
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card12.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("I ugly");
		}
		return cardInput;
	}

}
