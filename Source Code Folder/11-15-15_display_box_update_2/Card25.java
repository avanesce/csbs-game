import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card25 extends Card{
	String cardName = "Meet the Dean"; 
	int roomReq = 12;
	int roomReq2 = 15;
	int yearReq = 1;
	int[] reqPoints = {3,3,3,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p){
		if((p.getCurrentRoom()==roomReq || p.getCurrentRoom()==roomReq2)
			&& p.getLearn()>=reqPoints[0]
			&& p.getCraft()>=reqPoints[1]
			&& p.getInteg()>=reqPoints[2]){
			//ADD POINTS
			p.setQual(5);
			//DRAW CARD
			//PRINT SUCCESS
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 5 Quality Points.\n" + 
					 "Player gains an additional card.\n");
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//DISCARD CARD
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n" +
					 "Player must discard a card.\n");
		}
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card25.png");
			cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("I ugly");
		}
		return cardInput;
	}
}