import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card11 extends Card{
	String cardName = "Enjoying the Peace"; 
	int roomReq = 1;
	int yearReq = 1;
	int[] reqPoints = {0,0,0,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p){
		if(p.getCurrentRoom()==roomReq){
			//PROMPT LEARNING OR INTGRITY
			
			//LEARNING
			p.setLearn(1);// OR p.setInteg(1); 
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for and 1 Learning Chip.\n");
			
			//INTEGRITY
			p.setInteg(1); 
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for and 1 Integrity Chip.\n");
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n" );
		}
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card11.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("I ugly");
		}
		return cardInput;
	}

}
