import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import javax.swing.JOptionPane;


public class Card10 extends Card{
	String cardName = "Goodbye, Professor"; 
	int roomReq = 13;
	int yearReq = 1;
	int[] reqPoints = {6,6,6,0};
	CardChooser c;
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p) throws IOException{
		//AI CHECK - AUTO GIVE QUAL
		if (GameState.currentPlayer!=0){
			if (p.getCurrentRoom()==roomReq){
				p.setQual(10);
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						 + cardName + " for 10 Quality Points.\n" );
			}
			else{
				//TAKE POINTS
				p.setQual(-2);
				//PRINT FAIL
				Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
				 + cardName + " and lost 2 Quality Points.\n" +
				 "Player also loses this card and another from their hand.\n");
			}
		}
		//HUMAN
		else if(p.getCurrentRoom()==roomReq && p.getLearn()>=reqPoints[0]
			&& p.getCraft()>=reqPoints[1] && p.getInteg()>=reqPoints[2]){
			//ADD POINTS
			p.setQual(10);
			//PRINT SUCCESS
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 10 Quality Points.\n");
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//DISCARD A CARD
			c = new CardChooser(p);
			c.displayCardChooser();
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n" +
					 "Player also loses this card and another from their hand.\n");
		}
	}
	
	public String getName() {
		return ("Goodbye, Professor");
	}

	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card10.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("I ugly");
		}
		return cardInput;
	}

}
