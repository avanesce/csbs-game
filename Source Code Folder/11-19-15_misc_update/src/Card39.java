import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card39 extends Card{
	String cardName = "Student Parking"; 
	int roomReq = 2;
	int yearReq = 1;
	int[] reqPoints = {0,0,0,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p){
		if(p.getCurrentRoom()==roomReq){
			//ADD POINTS
			p.setCraft(1);
			//MOVE PLAYER
			p.setCurrentRoom(20);
			GameState.board.redrawPlayer(GameState.roomList[20], GameState.currentPlayer);
			GameState.board.redrawMoveList(GameState.roomList[20], GameState.roomList);
			//PRINT SUCCESS
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 4 Quality Points and 1 Crafting Chip.\n" +
					 "Player is moved to the Lactation Lounge.");
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n" );
		}
	}
	
	public String getName() {
		return ("Student Parking");
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card39.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("Card39.png does not exist.");
		}
		return cardInput;
	}

}
