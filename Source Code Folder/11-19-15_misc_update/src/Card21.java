import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card21 extends Card{
	String cardName = "Score a Goal!"; 
	int roomReq = 0;
	int yearReq = 1;
	int[] reqPoints = {0,3,0,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p){
		if((p.getCurrentRoom()==roomReq && p.getCraft()>=reqPoints[1])||(GameState.currentPlayer!=0 && p.getCurrentRoom()==roomReq)){
			//ADD POINTS
			p.setInteg(1);
			p.setQual(5);
			//PRINT SUCCESS
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 5 Quality Points and 1 Integrity Chip.\n");
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//MOVE PLAYER
			GameState.board.redrawPlayer(GameState.roomList[2], GameState.currentPlayer);
			GameState.board.redrawMoveList(GameState.roomList[2], GameState.roomList);
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n" +
					 "Player is moved to Student Parking.\n");
		}
	}
	
	
	public String getName() {
		return ("Score a Goal!");
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card21.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("Card21.png does not exist.");
		}
		return cardInput;
	}

}
