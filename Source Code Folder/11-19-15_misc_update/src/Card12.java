import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import javax.swing.JOptionPane;


public class Card12 extends Card{
	String cardName = "Buddy Up"; 
	int roomReq = 0;
	int roomReq2 = 18;
	int yearReq = 1;
	int[] reqPoints = {6,6,6,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p){
		//AI CHECK - AUTO GIVE CRAFT
		if (GameState.currentPlayer!=0 && p.getCurrentRoom()==roomReq){
			p.setCraft(1);
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 1 Crafting Chip.\n" );
		}
		//HUMAN
		else if(p.getCurrentRoom()==roomReq || p.getCurrentRoom()==roomReq2){
			//PROMPT LEARNING OR CRAFT
			String[] choices = { "Get 1 Learning Chip", "Get 1 Craft Chip"};
   		String input = (String) JOptionPane.showInputDialog(null, "Pick a Chip",
         	"Pick a Chip", JOptionPane.QUESTION_MESSAGE, null, choices, choices[1]);
			//LEARNING
			if(input.equals("Get 1 Learning Chip")){
			p.setLearn(1);
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 1 Learning Chip.\n");
			}
			//CRAFT
			else if(input.equals("Get 1 Craft Chip")){
			p.setCraft(1); 
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 1 Craft Chip.\n");
			}
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n");
		}
	}
	
	public String getName() {
		return ("Buddy Up");
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card12.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("Card12.png does not exist.");
		}
		return cardInput;
	}

}
