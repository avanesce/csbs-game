import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card16 extends Card{
	String cardName = "KIN 253"; 
	int roomReq = 0;
	int yearReq = 1;
	int[] reqPoints = {0,2,4,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p){
		if(GameState.currentPlayer!=0 && p.getCurrentRoom()==roomReq){
			//ADD POINTS
			p.setQual(4);
			//PRINT SUCCESS
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 4 Quality Points.\n");
		}
		else if(p.getCurrentRoom()==roomReq
			&& p.getCraft()>=reqPoints[1]
			&& p.getInteg()>=reqPoints[2]){
			//ADD POINTS
			p.setQual(4);
			//PRINT SUCCESS
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 4 Quality Points.\n");
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//MOVE PLAYER
			p.setCurrentRoom(13);
			GameState.board.redrawPlayer(GameState.roomList[13], GameState.currentPlayer);
			GameState.board.redrawMoveList(GameState.roomList[13], GameState.roomList);
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n" +
					 "Player is moved to the Room of Retirement.\n");
		}
	}
	
	
	public String getName() {
		return ("KIN 253");
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card16.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("Card16.png does not exist.");
		}
		return cardInput;
	}

}
