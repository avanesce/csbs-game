import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import javax.swing.JOptionPane;


public class Card37 extends Card{
	String cardName = "Make a Friend"; 
	int roomReq = 12;
	int roomReq2 = 15;
	int yearReq = 1;
	int[] reqPoints = {0,0,2,0};
	Board b;
	CardChooser c;
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p) throws IOException{
		
		if (GameState.currentPlayer!=0){
			if (p.getCurrentRoom()==roomReq){
				p.setLearn(1);
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						 + cardName + " for 1 Learning Chip.\n" );
			}
			else{
				//TAKE POINTS
				p.setQual(-2);
				//PRINT FAIL
				Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
				 + cardName + " and lost 2 Quality Points.\n" +
				 "Player must also discard a card.\n");
			}
		}
		else if((p.getCurrentRoom() == roomReq || p.getCurrentRoom() == roomReq2)
				&& p.getInteg() >= reqPoints[2]){
			p.setQual(7);
			//Insert popup for choose a chip
			String[] choices = { "Get 1 Learning Chip", "Get 1 Craft Chip", "Get 1 Integrity Chip"};
   		String input = (String) JOptionPane.showInputDialog(null, "Pick a Chip",
   				"Pick a Chip", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]); 
   		
			if(input.equals("Get 1 Learning Chip")) {
				p.setLearn(1);
				//PRINT SUCCESS
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						 + cardName + " for 1 Learning Chip.\n" );
				}
				//LEARNING
				else if(input.equals("Get 7 Quality Points and 1 Learning Chip")) {
				p.setInteg(1);
				//PRINT SUCCESS
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						 + cardName + " for 7 Quality Points and 1 Integrity Chip.\n" );
				}
				else if(input.equals("Get 1 Craft Chip")) {
					p.setCraft(1);
					//PRINT SUCCESS
					Board.currentPlayText.append(p.getPlayerName() + " has played " 
							 + cardName + " for 7 Quality Points and 1 Craft Chip.\n" );
					}
		}
		else{
			p.setQual(-2);
			//Player discard a card
			c = new CardChooser(p);
			c.displayCardChooser();
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points and must discard a card.\n" );
		}
	}
	
	public String getName() {
		return ("Make a Friend");
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card37.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("I ugly");
		}
		return cardInput;
	}

}
