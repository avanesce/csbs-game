import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card30 extends Card{
	String cardName = "Soccer Goalie"; 
	int roomReq = 0;
	int yearReq = 1;
	int[] reqPoints = {3,3,0,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p){
		if (GameState.currentPlayer!=0){
			if (p.getCurrentRoom()==roomReq){
				//ADD POINTS
				p.setQual(5);
				//PRINT SUCCESS
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						 + cardName + " for 5 Quality Points.\n" +
						 "Player gains an additional card.\n");
			}
			else{
				//TAKE POINTS
				p.setQual(-2);
				//MOVE PLAYER
				p.setCurrentRoom(2);
				GameState.board.redrawPlayer(GameState.roomList[2], GameState.currentPlayer);
				GameState.board.redrawMoveList(GameState.roomList[2], GameState.roomList);
				//PRINT FAIL
				Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
						 + cardName + " and lost 2 Quality Points.\n" +
						 "Player is moved to Student Parking.\n");
			}
		}
		else if(p.getCurrentRoom()==roomReq 
			&& p.getLearn()>=reqPoints[0]
			&& p.getCraft()>=reqPoints[1]){
			//ADD POINTS
			p.setQual(5);
			//DRAW CARD
			p.drawCard(GameState.deck);
			//PRINT SUCCESS
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 5 Quality Points.\n" +
					 "Player gains an additional card.\n");
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//MOVE PLAYER
			p.setCurrentRoom(2);
			GameState.board.redrawPlayer(GameState.roomList[2], GameState.currentPlayer);
			GameState.board.redrawMoveList(GameState.roomList[2], GameState.roomList);
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n" +
					 "Player is moved to Student Parking.\n");
		}
	}
	
	public String getName() {
		return ("Soccer Goalie");
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card30.png");
			cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("Card30.png does not exist.");
		}
		return cardInput;
	}
}