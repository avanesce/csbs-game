import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card38 extends Card{
	String cardName = "Enjoying Nature"; 
	String description;
	int roomReq = 11;
	int yearReq = 1;
	int[] reqPoints = {0,0,0,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p){
		if(p.getCurrentRoom() < roomReq){
			//ADD POINTS
			p.setCraft(1);
			//MOVE PLAYER
			p.setCurrentRoom(20);
			GameState.board.redrawPlayer(GameState.roomList[20], GameState.currentPlayer);
			//SUCCESS PRINT
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 1 Crafting Chip.\n" +
					 "Player is moved to the Lactation Lounge.");
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//FAIL PRINT
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n" );
		}
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card38.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("I ugly");
		}
		return cardInput;
	}

}
