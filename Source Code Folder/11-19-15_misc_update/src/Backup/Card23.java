import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card23 extends Card{
	String cardName = "Make the Dean's List"; 
	int roomReq = 12;
	int roomReq2 = 15;
	int yearReq = 1;
	int[] reqPoints = {6,0,0,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p){
		if((p.getCurrentRoom()==roomReq || p.getCurrentRoom()==roomReq2)
			&& p.getLearn()>=reqPoints[0]){
			//ADD POINTS
			p.setQual(5);
			//PRINT SUCCESS
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 5 Quality Points.\n");
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//MOVE PLAYER
			p.setCurrentRoom(2);
			GameState.board.redrawPlayer(GameState.roomList[2], GameState.currentPlayer);
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n" +
					 "Played is moved to Student Parking.\n");
		}
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card23.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("I ugly");
		}
		return cardInput;
	}

}
