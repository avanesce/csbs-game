/* CLASS BOARD
 * Function: Creates the game's visual interface
 * Provides objects for
 * 	Game Map
 * 	
 */

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;

import javax.imageio.*;
import javax.imageio.stream.ImageInputStream;
public class Board {
	//Master Window
	private static JFrame masterFrame = new JFrame("CSBS Challenge Game");
	private static JScrollPane topPanel;
	private static JPanel bottomPanel = new JPanel();
	//Top Board Components
	static JLabel boardImage, player1, player2, player3;
	//Bottom Interface Components
		//Buttons
	JButton drawCardButton = new JButton("Draw Card");
	JButton moveButton = new JButton("Move");
	JButton playCardButton = new JButton("Play Card");
	ActionListener buttonListener;
	
		//Room List
	JList roomList;
	String listHolder;
	JScrollPane roomListPane = new JScrollPane(roomList);
		//Card Image
	JLabel cardPicture;
	static int imageCounter = 0;
		//Score Board
	DefaultListModel model2 = new DefaultListModel();
	JList scoreboardList; 
	JScrollPane scoreboardPanel;
		//Current Play
	static JTextArea currentPlayText = new JTextArea();
	JScrollPane currentPlayPanel;
	
	
	/***************OVERALL SETUP FUNCTION***************/
	void createAndDrawGUI() throws IOException {
		/***************TOP PANEL SETUP***************/
		createBoard();
		/***************BOTTOM PANEL SETUP***************/
		createUI();
		//Add Top/Bot panel to master frame, sets settings
		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, topPanel, bottomPanel);
		splitPane.setDividerLocation(500);
		masterFrame.setContentPane(splitPane);
		masterFrame.setVisible(true);
		masterFrame.setResizable(true);
		masterFrame.setSize(1670,2000);
		masterFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	
	
	/***************COMPONENT SETUP***************/
	//Creates top half of window
	void createBoard() throws IOException{
		InputStream input = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/BoardImage.png");
		ImageInputStream imageInput = ImageIO.createImageInputStream(input);
	   boardImage = new JLabel(new ImageIcon(ImageIO.read(imageInput)));
		boardImage.add(player1);
		boardImage.add(player2);
		boardImage.add(player3);
		topPanel = new JScrollPane(boardImage,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	}
	
	//Creates bottom half of window
	void createUI() throws IOException{
		currentPlayText = new JTextArea(1,1);
		currentPlayPanel = new JScrollPane(currentPlayText, 
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, 
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scoreboardList = new JList(model2);
		scoreboardPanel = new JScrollPane(scoreboardList);
		
		moveButtonSetup();
		playButtonSetup(GameState.playerList[0]);
		cardPictureSetup();
	
		bottomPanel.setLayout(null);
		bottomPanel.add(cardPicture);
		bottomPanel.add(currentPlayPanel);
		bottomPanel.add(scoreboardPanel);
		bottomPanel.add(roomListPane);
		bottomPanel.add(drawCardButton);
		bottomPanel.add(moveButton);
		bottomPanel.add(playCardButton);
		//Bottom panel configurations
		Insets insets = bottomPanel.getInsets();
		insets.top += 15;
		insets.left += 25;
		
		Dimension size = drawCardButton.getPreferredSize();
		drawCardButton.setBounds(insets.left, insets.top,
			size.width + 60, size.height);
		
		size = moveButton.getPreferredSize();
		moveButton.setBounds(insets.left, insets.top + 25,
			size.width + 90 , size.height);
		
		size = playCardButton.getPreferredSize();
		playCardButton.setBounds(insets.left, insets.top + 50,
			size.width + 66 , size.height );
		
		size = roomListPane.getPreferredSize();
		roomListPane.setBounds(insets.left, insets.top + 75,
			size.width + 94, size.height + 50);
		
		size = cardPicture.getPreferredSize();
		cardPicture.setBounds(insets.left + 175, insets.top,
				size.width, size.height);
		
		size = scoreboardPanel.getPreferredSize();
		scoreboardPanel.setBounds(insets.left + 400, insets.top,
			size.width + 600 , size.height + 50);
		
		size = currentPlayPanel.getPreferredSize();
		currentPlayPanel.setBounds(insets.left + 400, insets.top + 130,
			size.width + 830 , size.height + 120);
	}
	
	
	
	/***************COMPONENT ACTIONS***************/
	/* Function creates the button listener for moveButton
	 * Updates adjacent room list and player location when activated
	 */
	void moveButtonSetup(){
		roomList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		roomList.addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent event){
				listHolder = (String) roomList.getSelectedValue();
			}
		});
		
		moveButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				//String selectedRoom = listHolder.contains("South Hall");
				Room tempRoom = null;
				int j = 0;
				for(int i = 0; i < 21; i++){
					if(GameState.roomList[i].getRoomName().equals(listHolder)){
						tempRoom = GameState.roomList[i];
						j = i;
					}
				}
				switch(GameState.currentPlayer){
				case 0:
					redrawPlayer(tempRoom, GameState.currentPlayer);
					break;
				case 1:
					redrawPlayer(tempRoom, GameState.currentPlayer);
					break;
				case 2:	
					redrawPlayer(tempRoom, GameState.currentPlayer);
					break;
				}
				GameState.playerList[GameState.currentPlayer].setCurrentRoom(j);
				
				redrawMoveList(tempRoom, GameState.roomList);
				roomList.repaint();
				Player tempPlayer =  GameState.playerList[GameState.currentPlayer];
				currentPlayText.append(tempPlayer.getPlayerName() + " has moved to " 
				 + GameState.roomList[tempPlayer.getCurrentRoom()].getRoomName() + "\n");
				GameState.currentPlayer++; //TESTING PURPOSES ONLY, REMOVE LATER
			}
		});
	}
	
	// Function sets up the play card button
	void playButtonSetup(Player p){
		 playCardButton.addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e){
	         	String cardName = p.hand.get(imageCounter).getName();
	         	if(cardName == "Math 122") {
	         		String[] choices = { "Get 1 Learning Chip", "Get 1 Integrity Chip"};
	               String input = (String) JOptionPane.showInputDialog(null, "Choose a Chip NOOB",
	                   "The Choice of a Lifetime", JOptionPane.QUESTION_MESSAGE, null, choices, choices[1]);
	               //p.currentCardHand.play(p,input);
	               System.out.println(input);
	         	}
	         	else if(cardName == "Enjoying the Peace") {
	         		String[] choices = { "Get 1 Learning Chip", "Get 1 Integrity Chip"};
	               String input = (String) JOptionPane.showInputDialog(null, "Choose a Chip NOOB",
	                   				"Pick a Card", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]); 
	               System.out.println(input);
	         	}
	         	else if(cardName == "Buddy Up") {
	         		String[] choices = { "Get 1 Learning Chip", "Get 1 Craft Chip"};
	               String input = (String) JOptionPane.showInputDialog(null, "Choose a Chip NOOB",
	                   				"Pick a Card", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]); 
	               System.out.println(input);
	         	}
	         	else if(cardName == "A New Laptop") {
	         		String[] choices = { "Get 1 Learning Chip", "Get 1 Craft Chip", "Get 1 Integrity Chip"};
	               String input = (String) JOptionPane.showInputDialog(null, "Choose a Chip NOOB",
	                   				"Pick a Card", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]); 
	               System.out.println(input);
	         	}
	         	else if(cardName == "Loud Buzzing") {
	         		String[] choices = { "Get 1 Learning Chip", "Get 1 Craft Chip", "Get 1 Integrity Chip"};
	               String input = (String) JOptionPane.showInputDialog(null, "Choose a Chip NOOB",
	                   				"Pick a Card", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]); 
	               System.out.println(input);
	         	}
	         	else if(cardName == "Program Crashes") {
	         		String[] choices = { "Get 1 Learning Chip", "Get 1 Craft Chip", "Get 1 Integrity Chip"};
	               String input = (String) JOptionPane.showInputDialog(null, "Choose a Chip NOOB",
	                   				"Pick a Card", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]); 
	               System.out.println(input);
	         	}
	         	else if(cardName == "Professor Englert") {
	         		String[] choices = { "Get 1 Learning Chip", "Get 1 Craft Chip", "Get 1 Integrity Chip"};
	               String input = (String) JOptionPane.showInputDialog(null, "Choose a Chip NOOB",
	                   				"Pick a Card", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]); 
	               System.out.println(input);
	         	}
	         	else if(cardName == "Oral Communication") {
	         		String[] choices = { "Get 1 Learning Chip", "Get 1 Craft Chip", "Get 1 Integrity Chip"};
	               String input = (String) JOptionPane.showInputDialog(null, "Choose a Chip NOOB",
	                   				"Pick a Card", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]); 
	               System.out.println(input);
	         	}
	         	else if(cardName == "The Outpost") {
	         		String[] choices = { "Get 1 Learning Chip", "Get 1 Craft Chip", "Get 1 Integrity Chip"};
	               String input = (String) JOptionPane.showInputDialog(null, "Choose a Chip NOOB",
	                   				"Pick a Card", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]); 
	               System.out.println(input);
	         	}
	         	else if(cardName == "Learning Linux") {
	         		String[] choices = { "Get 1 Learning Chip", "Get 1 Craft Chip", "Get 1 Integrity Chip"};
	               String input = (String) JOptionPane.showInputDialog(null, "Choose a Chip NOOB",
	                   				"Pick a Card", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]); 
	               System.out.println(input);
	         	}
	         	else if(cardName == "Make a Friend") {
	         		String[] choices = { "Get 1 Learning Chip", "Get 1 Craft Chip", "Get 1 Integrity Chip"};
	               String input = (String) JOptionPane.showInputDialog(null, "Choose a Chip NOOB",
	                   				"Pick a Card", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]); 
	               System.out.println(input);
	         	}    
	         	//else
	         		//p.currentCardHand.play(p);
	         }
	         });
	}
	
	// Function creates the card picture label and event listener
	void cardPictureSetup() throws IOException{
		final Player player = GameState.playerList[0];
		cardPicture = new JLabel(new ImageIcon(ImageIO.read(player.hand.get(0).displayPicture())));
		cardPicture.addMouseListener(new MouseAdapter(){  
			public void mouseClicked(MouseEvent e){ 	   	
				try {  
					do{
						imageCounter++;
						if(imageCounter>player.hand.size()-1) {
							imageCounter = 0;
							cardPicture.setIcon(new ImageIcon(ImageIO.read(player.hand.get(imageCounter).displayPicture())));
						}
						else
							cardPicture.setIcon(new ImageIcon(ImageIO.read(player.hand.get(imageCounter).displayPicture())));
					}while(player.hand.equals(null));
			
					}
				catch (IOException e1) {
					e1.printStackTrace();
				}
	      }  
	  }); 
	}

	//Function creates the INITIAL list of adjacent rooms displayed
	public void createMoveList(Room room, Room[] roomArray){
		DefaultListModel<String> model = new DefaultListModel<>();
		for(int i = 0; i < room.adjacentRooms.length; i++){
			model.addElement(roomArray[room.adjacentRooms[i]].getRoomName());
		}
		roomList = new JList(model);
		roomListPane = new JScrollPane(roomList);
	}
	
	//Function UPDATES the list of adjacent rooms displayed
	public void redrawMoveList(Room room, Room[] roomArray){
		DefaultListModel<String> model = new DefaultListModel<>();
		for(int i = 0; i < room.adjacentRooms.length; i++){
			model.addElement(roomArray[room.adjacentRooms[i]].getRoomName());
		}
		roomList.setModel(model);
	}
	

	//Function creates the INITIAL set of player tokens
	public void createPlayerLabel(String playerName, int i){
		if(i==0){
			player1 = new JLabel(playerName);
			playerTokenSetup(player1, i);
		}
		else if(i==1){
			player2 = new JLabel(playerName);
			playerTokenSetup(player2, i);
		}
		else{
			player3 = new JLabel(playerName);
			playerTokenSetup(player3, i);
		}
	}
	
	//Function changes the INITIAL settings of player tokens
	public void playerTokenSetup(JLabel label, int i){
		label.setFont(player1.getFont().deriveFont(Font.BOLD, 34));
		label.setForeground(Color.RED);
		label.setSize(150, 30);
	}
	
	//Function UPDATES the player tokens
	public static void redrawPlayer(Room room, int i){
		int posX = room.getPosX();	
		int posY = room.getPosY() + (i * 40);
		if(i == 0)
			player1.setLocation(posX, posY);
		else if(i == 1)
			player2.setLocation(posX, posY);
		else
			player3.setLocation(posX, posY);
	}
}
