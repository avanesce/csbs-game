import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card8 extends Card{
	String cardName = "Parking Violation"; 
	int roomReq = 6;
	int yearReq = 1;
	int[] reqPoints = {0,0,0,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p){
		if(p.getCurrentRoom()==roomReq){
			//ADD POINTS
			p.setLearn(2);
			//PRINT SUCCESS
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 2 Learning Chips.\n" +
					 "Player discards a card for an extra point.");
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n" );
		}
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card8.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("I ugly");
		}
		return cardInput;
	}

}
