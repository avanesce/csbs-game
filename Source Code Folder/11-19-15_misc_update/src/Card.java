import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;

/* CLASS CARD
 * Function: Determine characteristics of each card.
 *  Cards are used in play to simulate activities and
 *  to advance the game state.
 */

/*
 * TO BE IMPLEMENTED IN UPCOMING ITERATIONS
 */
public abstract class Card {
	String cardName;
	int roomReq; //subclasses may have additional rooms
	int yearReq;
	int[] reqPoints = {0,0,0,0}; //{LEARNING, CRAFTING, INTEGRITY, QUALITY}
	int qpPenalty;
	
	//Each Card has a different method that returns values to the caller
	//The values are an int array of 3 slots that contains points
	//The values are Learning, Crafting, and Integrity
	abstract void play(Player p) throws IOException;
	abstract ImageInputStream displayPicture();
	abstract String getName();
}
