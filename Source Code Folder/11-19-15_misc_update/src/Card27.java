import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import javax.swing.JOptionPane;


public class Card27 extends Card{
	String cardName = "Program Crashes"; 
	int roomReq = 27;
	int yearReq = 1;
	int[] reqPoints = {2,0,0,0};
	CardChooser c;
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p) throws IOException{
		
		//AI CHECK - AUTO GIVE INTEG
		if (GameState.currentPlayer!=0){
			if (p.getCurrentRoom()==roomReq){
				p.setInteg(1);
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						 + cardName + " for 1 Integrity Chip.\n" );
			}
			else{
				//TAKE POINTS
				p.setQual(-2);
				//PRINT FAIL
				Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
				 + cardName + " and lost 2 Quality Points.\n" +
				 "Player must also discard a card.\n");
			}
		}
		//HUMAN
		else if(p.getCurrentRoom()==roomReq && p.getLearn()>=reqPoints[0]){
			//ADD POINTS
			p.setQual(5);
			//CHOICE OF CHIP
			String[] choices = { "Get 1 Learning Chip", "Get 1 Craft Chip", "Get 1 Integrity Chip"};
   		String input = (String) JOptionPane.showInputDialog(null, "Pick a Chip",
   				"Pick a Chip", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]);
			if(input.equals("Get 1 Learning Chip")) {
				p.setLearn(1);
				//PRINT SUCCESS
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						 + cardName + " for 1 Learning Chip.\n" );
				}
				//LEARNING
				else if(input.equals("Get 1 Learning Chip")) {
				p.setInteg(1);
				//PRINT SUCCESS
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						 + cardName + " for 1 Integrity Chip.\n" );
				}
				else if(input.equals("Get 1 Craft Chip")) {
					p.setCraft(1);
					//PRINT SUCCESS
					Board.currentPlayText.append(p.getPlayerName() + " has played " 
							 + cardName + " for 1 Craft Chip.\n" );
					}
			//PRINT SUCCESS
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 5 Quality Points and 1 selected chip.\n");
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//DISCARD CARD
			c = new CardChooser(p);
			c.displayCardChooser();
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n" +
					 "Player must discard a card.\n");
		}
	}
	
	
	public String getName() {
		return ("Program Crashes");
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card27.png");
			cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("I ugly");
		}
		return cardInput;
	}
}