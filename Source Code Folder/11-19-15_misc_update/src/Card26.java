import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import javax.swing.JOptionPane;


public class Card26 extends Card{
	String cardName = "Loud Buzzing"; 
	int roomReq = 18;
	int yearReq = 1;
	int[] reqPoints = {0,3,0,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p){
		//AI CHECK - AUTO GIVE CRAFT
		if (GameState.currentPlayer!=0){
			if (p.getCurrentRoom()==roomReq){
				p.setCraft(4);
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						 + cardName + " for 4 Crafting Chip.\n" );
			}
			else{
				//TAKE POINTS
				p.setQual(-4);
				//PRINT FAIL
				Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
				 + cardName + " and lost 4 Quality Points.\n");
			}
		}
		//HUMAN
		else if(p.getCurrentRoom()==roomReq && p.getCraft()>=reqPoints[1]){
			//ADD POINTS
			//GET CHIP OF CHOICE
			String[] choices = { "Get 1 Learning Chip", "Get 1 Craft Chip", "Get 1 Integrity Chip"};
   		String input = (String) JOptionPane.showInputDialog(null, "Pick a Chip",
   				"Pick a Chip", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]);
   		
   		//LEARNING
			if(input.equals("Get 1 Learning Chip")) {
				p.setLearn(1);
				p.setCraft(3);
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 3 Craft Chips and 1 Learning Chip.\n" );
				}
			//INTEGRITY
			else if(input.equals("Get 1 Integrity Chip")) {
				p.setInteg(1);
				p.setCraft(3);
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 3 Craft Chips and 1 Integrity Chip.\n" );
			}
			//CRAFT
			else if(input.equals("Get 1 Craft Chip")) {
				p.setCraft(4);
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 4 Craft Chips.\n" );
			}
		}
		else{
			//TAKE POINTS
			p.setQual(-4);
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 4 Quality Points.\n" );
		}
	}
	
	public String getName() {
		return ("Loud Buzzing");
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card26.png");
			cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("Card26.png does not exist.");
		}
		return cardInput;
	}
}