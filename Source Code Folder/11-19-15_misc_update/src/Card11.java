import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import javax.swing.JOptionPane;


public class Card11 extends Card{
	String cardName = "Enjoying the Peace"; 
	int roomReq = 1;
	int yearReq = 1;
	int[] reqPoints = {0,0,0,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p){
		//AI CHECK -- AUTOGIVE INTEGRITY
		if (GameState.currentPlayer!=0 && p.getCurrentRoom()==roomReq){
			p.setInteg(1);
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 1 Integrity Chip.\n" );
		}
		//HUMAN
		else if(p.getCurrentRoom()==roomReq){
			//PROMPT LEARNING OR INTGRITY
			String[] choices = { "Get 1 Learning Chip", "Get 1 Integrity Chip"};
   		String input = (String) JOptionPane.showInputDialog(null, "Pick a Chip",
         	"Pick a Chip", JOptionPane.QUESTION_MESSAGE, null, choices, choices[1]);
			//LEARNING
			if(input.equals("Get 1 Learning Chip")) {
				p.setLearn(1); 
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						 + cardName + " for and 1 Learning Chip.\n");
			}
			//INTEGRITY
			else if(input.equals("Get 1 Integrity Chip")){
				p.setInteg(1); 
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						+ cardName + " for and 1 Integrity Chip.\n");
			}
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n" );
		}
	}
	
	public String getName() {
		return ("Enjoying the Peace");
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card11.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("Card11.png does not exist.");
		}
		return cardInput;
	}

}
