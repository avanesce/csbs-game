import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;


public class CardChooser {
	JFrame mFrame = new JFrame();
	JPanel cardPanel = new JPanel();
	JButton discardButton = new JButton();
	JLabel cardPicture = new JLabel();
	int imageCounter=0;
	
	public CardChooser(final Player p) throws IOException {
		cardPicture = new JLabel(new ImageIcon(ImageIO.read(p.hand.get(0).displayPicture())));
		discardButton = new JButton("Discard");
		cardPanel.add(cardPicture);
		cardPanel.add(discardButton);
		discardButton.setLocation(10, 200);
		mFrame.add(cardPanel);
		
		Insets insets = cardPanel.getInsets();
		insets.top += 15;
		insets.left += 25;
		
		Dimension size = discardButton.getPreferredSize();
		discardButton.setBounds(insets.left, insets.top,
			size.width + 60, size.height);
		
		size = cardPicture.getPreferredSize();
		cardPicture.setBounds(insets.left, insets.top + 75,
			size.width + 94, size.height + 50);
		
		p.setCurrentCardHand(p.hand.get(0));
		cardPicture.addMouseListener(new MouseAdapter(){  
			public void mouseClicked(MouseEvent e){ 	   	
				try {  
					do{
						imageCounter++;
						if(imageCounter>p.hand.size()-1) {
							imageCounter = 0;
							cardPicture.setIcon(new ImageIcon(ImageIO.read(p.hand.get(imageCounter).displayPicture())));
							p.setCurrentCardHand(p.hand.get(imageCounter));
						}
						else{
							cardPicture.setIcon(new ImageIcon(ImageIO.read(p.hand.get(imageCounter).displayPicture())));
							p.setCurrentCardHand(p.hand.get(imageCounter));
						}
					}while(p.hand.equals(null));
			
					}
				catch (IOException e1) {
					e1.printStackTrace();
				}
	      }  
	  }); 
		
		discardButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				for(int i = 0; i < p.hand.size(); i++){
					if(p.hand.get(i).getName() == p.hand.get(imageCounter).getName()){
	              	p.discardCard(i);
	              	break;
					}
					GameState.board.updateScoreboard();
				}
				mFrame.dispose();
			}
			});
	}

	public JFrame displayCardChooser() {
		mFrame.setVisible(true);
		mFrame.setContentPane(cardPanel);
		mFrame.setVisible(true);
		mFrame.setResizable(true);
		mFrame.setSize(240, 350);
		return mFrame;
	}
}

