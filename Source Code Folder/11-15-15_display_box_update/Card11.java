import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;

//The values are Learning, Crafting, and Integrity
public class Card11 extends Card{
	private ImageInputStream cardInput;
	public void play(Player p){
		if(p.getCurrentRoom()==7) {
			//Get one learning pt or one integrity pt
			p.setLearningPt(p.getLearnPt() +1);
		}
	}
	
	public String getName() {
		return ("Enjoying the Peace");
	}
	
	public ImageInputStream displayPIcture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Randy/Desktop/Cards/Card11.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("I ugly");
		}
		return cardInput;
	}}