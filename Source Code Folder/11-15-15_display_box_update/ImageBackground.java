import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.imageio.*;
import javax.imageio.stream.ImageInputStream;
public class ImageBackground {
	static String listHolder;
	static Room tempRoom;
	static JScrollPane pane;
	static int i=0; //Create an integer and set it too 0 that you'll use to access the array index
	public static void main(String[] args) throws IOException {
		
		JFrame F = new JFrame("CSBS Background Frame");
		InputStream input = new FileInputStream("C:/Users/Randy/Desktop/CSULBMap3.png");
	   ImageInputStream imageInput = ImageIO.createImageInputStream(input);
	   JPanel imagePanel = new JPanel();
	   //Creates a label to store the CSULBMap.png image
	   JLabel background = new JLabel(new ImageIcon(ImageIO.read(imageInput)));
	   background.setLayout(null);
	   imagePanel.add(background);
	   
	   final JLabel label = new JLabel("Player 1");
	   label.setFont(label.getFont().deriveFont(Font.BOLD, 14));
	   label.setForeground(Color.RED);
	   label.setLocation(180,1170);
	  label.setSize(500,100);
	   background.add(label);
	   imagePanel.add(background);
	   
	   
	  
	   JScrollPane topLabelCSULBMAP = new JScrollPane(imagePanel,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
     
      final JPanel leftPanel = new JPanel();
      leftPanel.setLayout(null);

      final JButton drawBUTTON = new JButton("Draw Card");
      JButton moveBUTTON = new JButton("Move");
      JButton playBUTTON = new JButton("Play Card");
      
	   
	   JTextArea commentTextArea = new JTextArea();
	   commentTextArea = new JTextArea(1,1); 
		JScrollPane textArea1 = new JScrollPane(commentTextArea, 
															ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, 
															ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER); 
		
		DefaultListModel model = new DefaultListModel();
	   final JList list = new JList(model);
	   pane = new JScrollPane(list);
	   
	DefaultListModel model2 = new DefaultListModel();
	
	//DEFINE PLAYER ROOM TO START PROGRAM
   final Room r = new Room("North Hall", 180, 1170, new int[]{4,11,14,13,16,15});
   final DefaultListModel<String> listModel = new DefaultListModel<>();
   final DefaultListModel<String> listModel2 = new DefaultListModel<>();
   for(int i=0; i<r.adjacentRooms.length; i++) {
   	int[] ra = r.adjacentRooms;
   	Room ra2 = new Room();
   	listModel.addElement(ra2.getAdjacentRoomSTR(r.adjacentRooms[i]));
   }
   final JList list2 = new JList<>(listModel);
   JScrollPane commentTextArea2 = new JScrollPane();
   pane = new JScrollPane(list2);
   
//UPDATE: 11/14/15___________________________________________________________________________
//Creates the Card Deck and a Label that stores the card images. 
//Creates the mouseClick listener that changes the card after every click
//In this example, only used 3 cards
//UPDATE: 11/15/15
//Had to modify the code where the deck and cards are List<Card>. So, an array of Cards
//Created a player, a gamestate for the deck, and the card image is displayed just fine with 
   final Player p = new Player();
   final GameState g = new GameState();
   g.createDeck();
   p.drawCard(g.deck);
   p.drawCard(g.deck);
   p.drawCard(g.deck);
   p.drawCard(g.deck);
   p.drawCard(g.deck);
  
   
  ///final Card[] card = new Card[3];
   //card[0] = new Card1();
   //card[1] = new Card2();
  // card[2] = new Card3();
     
  final JLabel cardPicture = new JLabel(new ImageIcon(ImageIO.read(p.hand.get(0).displayPIcture())));
  cardPicture.addMouseListener(new MouseAdapter() 
  {  
      public void mouseClicked(MouseEvent e)  
      { 	   	
     	try {  
     		do{
     		i++;
     		if(i>p.hand.size()-1) {
     			i=0;
     			cardPicture.setIcon(new ImageIcon(ImageIO.read(p.hand.get(i).displayPIcture())));
     			p.setCurrentCardHand(p.hand.get(i));
     		//cardPicture.setIcon(new ImageIcon(ImageIO.read(card[i].displayPIcture())));
     		}
     		else
     			//cardPicture.setIcon(new ImageIcon(ImageIO.read(card[i].displayPIcture())));
     			cardPicture.setIcon(new ImageIcon(ImageIO.read(p.hand.get(i).displayPIcture())));
     			p.setCurrentCardHand(p.hand.get(i));
     		}while(p.hand.equals(null));
					
     	}
     	catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
      	Insets insets = leftPanel.getInsets();
      	Dimension size = cardPicture.getPreferredSize();
      	cardPicture.setBounds(150 + insets.left,  + insets.top,
            size.width  , size.height);
      }  
  }); 
  
  //_______________________________________________________________________________________________
  
  		leftPanel.add(cardPicture);
		leftPanel.add(commentTextArea2);
		leftPanel.add(textArea1); 
	   leftPanel.add(pane);
      leftPanel.add(drawBUTTON);
      leftPanel.add(moveBUTTON);
      leftPanel.add(playBUTTON);
      //leftPanel.add(cardPicture);
      
      Insets insets = leftPanel.getInsets();
      
      Dimension size = drawBUTTON.getPreferredSize();
      drawBUTTON.setBounds(0 + insets.left, 0 + insets.top,
                   size.width, size.height);
      
      size = cardPicture.getPreferredSize();
      cardPicture.setBounds(150 + insets.left,  + insets.top,
         size.width  , size.height);

      size = moveBUTTON.getPreferredSize();
      moveBUTTON.setBounds(0 + insets.left, 25 + insets.top,
                   size.width , size.height);
      
      size = playBUTTON.getPreferredSize();
      playBUTTON.setBounds(0 + insets.left, 50 + insets.top,
                   size.width , size.height );
      
      size = pane.getPreferredSize();
      pane.setBounds(0 + insets.left, 75 + insets.top,
                   size.width , size.height );
      
      size = pane.getPreferredSize();
      textArea1.setBounds(500 + insets.left, 130 + insets.top,
                   size.width +600 , size.height -65);
      
      size = pane.getPreferredSize();
      commentTextArea2.setBounds(500 + insets.left, 10 + insets.top,
                   size.width +600 , size.height -20);
      
    
//_____________________________________________________________________________________________ 
//UPDATE: 11/15/15 8:37PM
      //ADDING PLAYBUTTON ACTION LISTENER
      
      playBUTTON.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e)
         {    
         	if(p.currentCardHand.getName() == "Math 122") {
         		String[] choices = { "Get 1 Learning Chip", "Get 1 Integrity Chip"};
               String input = (String) JOptionPane.showInputDialog(null, "Choose a Chip NOOB",
                   "The Choice of a Lifetime", JOptionPane.QUESTION_MESSAGE, null, // Use
                                                                                   // default
                                                                                   // icon
                   choices, // Array of choices
                   choices[1]); // Initial choice
               //input variable stores user's choice!!!!!
               
               //p.currentCardHand.play(p,input);
               System.out.println(input);
         	}
         	else if(p.currentCardHand.getName() == "Enjoying the Peace") {
         		String[] choices = { "Get 1 Learning Chip", "Get 1 Integrity Chip"};
               String input = (String) JOptionPane.showInputDialog(null, "Choose a Chip NOOB",
                   				"Pick a Card", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]); 
               System.out.println(input);
         	}
         	else if(p.currentCardHand.getName() == "Buddy Up") {
         		String[] choices = { "Get 1 Learning Chip", "Get 1 Craft Chip"};
               String input = (String) JOptionPane.showInputDialog(null, "Choose a Chip NOOB",
                   				"Pick a Card", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]); 
               System.out.println(input);
         	}
         	else if(p.currentCardHand.getName() == "A New Laptop") {
         		String[] choices = { "Get 1 Learning Chip", "Get 1 Craft Chip", "Get 1 Integrity Chip"};
               String input = (String) JOptionPane.showInputDialog(null, "Choose a Chip NOOB",
                   				"Pick a Card", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]); 
               System.out.println(input);
         	}
         	else if(p.currentCardHand.getName() == "Loud Buzzing") {
         		String[] choices = { "Get 1 Learning Chip", "Get 1 Craft Chip", "Get 1 Integrity Chip"};
               String input = (String) JOptionPane.showInputDialog(null, "Choose a Chip NOOB",
                   				"Pick a Card", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]); 
               System.out.println(input);
         	}
         	else if(p.currentCardHand.getName() == "Program Crashes") {
         		String[] choices = { "Get 1 Learning Chip", "Get 1 Craft Chip", "Get 1 Integrity Chip"};
               String input = (String) JOptionPane.showInputDialog(null, "Choose a Chip NOOB",
                   				"Pick a Card", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]); 
               System.out.println(input);
         	}
         	else if(p.currentCardHand.getName() == "Professor Englert") {
         		String[] choices = { "Get 1 Learning Chip", "Get 1 Craft Chip", "Get 1 Integrity Chip"};
               String input = (String) JOptionPane.showInputDialog(null, "Choose a Chip NOOB",
                   				"Pick a Card", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]); 
               System.out.println(input);
         	}
         	else if(p.currentCardHand.getName() == "Oral Communication") {
         		String[] choices = { "Get 1 Learning Chip", "Get 1 Craft Chip", "Get 1 Integrity Chip"};
               String input = (String) JOptionPane.showInputDialog(null, "Choose a Chip NOOB",
                   				"Pick a Card", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]); 
               System.out.println(input);
         	}
         	else if(p.currentCardHand.getName() == "The Outpost") {
         		String[] choices = { "Get 1 Learning Chip", "Get 1 Craft Chip", "Get 1 Integrity Chip"};
               String input = (String) JOptionPane.showInputDialog(null, "Choose a Chip NOOB",
                   				"Pick a Card", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]); 
               System.out.println(input);
         	}
         	else if(p.currentCardHand.getName() == "Learning Linux") {
         		String[] choices = { "Get 1 Learning Chip", "Get 1 Craft Chip", "Get 1 Integrity Chip"};
               String input = (String) JOptionPane.showInputDialog(null, "Choose a Chip NOOB",
                   				"Pick a Card", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]); 
               System.out.println(input);
         	}
         	else if(p.currentCardHand.getName() == "Make a Friend") {
         		String[] choices = { "Get 1 Learning Chip", "Get 1 Craft Chip", "Get 1 Integrity Chip"};
               String input = (String) JOptionPane.showInputDialog(null, "Choose a Chip NOOB",
                   				"Pick a Card", JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]); 
               System.out.println(input);
         	}    
         	//else
         		//p.currentCardHand.play(p);
         }
         });
         
      
      //_________________________________________________________________________________________________
      //Creates the LIST LISTENER 
      list2.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      list2.addListSelectionListener(new ListSelectionListener() {
      	public void valueChanged(ListSelectionEvent event) {
      		 listHolder = (String) list2.getSelectedValue();
      	}
      });
      
      
      //MOVE BUTTON LISTENER
      moveBUTTON.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e)
         {
         	if(listHolder.contains("South Hall")){
         		label.setLocation(830,1170);
         		tempRoom = new Room("South Hall", 830, 1170, new int[]{14,18,19,17,20,10});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("George Allen Field")) {
         		label.setLocation(20,20);
         		tempRoom = new Room("George Allen Field", 20, 20, new int[]{1,5,3,4});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Japanese Garden")) {
         		label.setLocation(430,20);       
         		tempRoom = new Room("Japanese Garden", 430, 20, new int[]{0,3,2});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Student Parking")) {
         		label.setLocation(920,20);
         		tempRoom = new Room("Student Parking", 920, 20, new int[]{1,3,5,6});
         		
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("The Pyramid")) {
         		label.setLocation(430,300);
         		tempRoom = new Room("The Pyramid", 430, 300, new int[]{0,1,2,5});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("West Walkway")) {
         		label.setLocation(20, 670);
         		tempRoom = new Room("West Walkway", 20, 670, new int[]{0,5,12,7});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Health Center")) {
         		label.setLocation(450,540);
         		tempRoom = new Room("Health Center", 450, 540, new int[]{0,3,4,2,6});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Forbidden Parking")) {
         		label.setLocation(1020,520);
         		tempRoom = new Room("Forbidden Parking", 1020, 520, new int[]{2,5,10});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Library")) {
         		label.setLocation(20,1540);
         		tempRoom = new Room("Library", 20, 1540, new int[]{4,8});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Union")) {
         		label.setLocation(480,1640);
         		tempRoom =  new Room("Union", 480, 1640, new int[]{7,9,16});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Bratwurst Hall")) {
         		label.setLocation(1050,1660);
         		tempRoom = new Room("Bratwurst Hall", 1050, 1660, new int[]{7,10});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("East Walkway")) {
         		label.setLocation(1480,960);
         		tempRoom = new Room("East Walkway", 1480, 960, new int[]{9,6,15});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Lab")) {
         		label.setLocation(180,890);
         		tempRoom = new Room("Lab", 180, 890, new int[]{12});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("North Hall")) {
         		label.setLocation(180,1170); 
         		tempRoom = new Room("North Hall", 180, 1170, new int[]{4,11,14,13,16,15});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3); 
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Room of Retirement")) {
         		label.setLocation(180,1360);
         		tempRoom =new Room("Room of Retirement", 180, 1360, new int[]{12});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3); 
         	   pane.repaint();
         	}
         	else if(listHolder.contains("302")) {
         		label.setLocation(620,890);
         		tempRoom =new Room("302", 620, 890, new int[]{12,15});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3); 
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Elevators")) {
         		label.setLocation(620,1360);
         		tempRoom = new Room("Elevators", 620, 1360, new int[]{12,8});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3);
         	   pane.repaint();
         	}
         	else if(listHolder.contains("308")) {
         		label.setLocation(830,1360);

         		tempRoom =new Room("308", 830, 1360, new int[]{15});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3); 
         	   pane.repaint();
         	}
         	else if(listHolder.contains("EAT")) {
         		label.setLocation(1050,890);
         		tempRoom =new Room("EAT", 1050, 890, new int[]{15});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3); 
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Conference")) {
         		label.setLocation(1260,890);
         		tempRoom =new Room("Conference", 1260, 890, new int[]{15});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3); 
         	   pane.repaint();
         	}
         	else if(listHolder.contains("Noisy Room")) {
         		label.setLocation(1230,1360);
         		tempRoom =new Room("Noisy Room", 1230, 1360, new int[]{15});
         		listModel.removeAllElements();
         	   for(int i=0; i<tempRoom.adjacentRooms.length; i++) {
         	   	int[] ra = tempRoom.adjacentRooms;
         	   	Room ra2 = new Room();
         	   	listModel.addElement(ra2.getAdjacentRoomSTR(tempRoom.adjacentRooms[i]));
         	   }
         	  JList<String> list3 = new JList<>(listModel);
         	   JScrollPane commentTextArea2 = new JScrollPane();
         	   pane = new JScrollPane(list3); 
         	   pane.repaint();
         	}
 
         }
     });      
      
      //End of Actionlisteners 
      //________________________________________________________________________________________________
      
      
     
      
      
     
      JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, topLabelCSULBMAP, leftPanel);
      splitPane.setDividerLocation(500);
      F.setContentPane(splitPane);
		F.setVisible(true);
		F.setResizable(true);
		F.setSize(1670,2000);
		F.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}
