/* CLASS ROOM
 * Function: Room contains final values for game
 *  objects that relate to the position of rooms.
 *  Values are used by players, card, and the board
 *  for determining movement and position.
 */
public class Room {
	String roomName;
	int posX;
	int posY;
	int[] adjacentRooms;
	Room[] r;
	int x;
	
	//Default Constructor
	public Room(){
		
	}
	//Constructor with value assignment
	public Room(String newRoomName, int newPosX, int newPosY, int[] adjRoom){
		roomName = newRoomName;
		posX = newPosX;
		posY = newPosY;
		adjacentRooms = adjRoom;
	}
	/************************************************/
	/***********Standard GET/SET FUCNTIONS***********/
	/************************************************/
	//SET functions are not included as all room values are final
	String getRoomName(){
		return roomName;
	}
	
	int getPosX(){
		return posX;
	}
	
	int getPosY(){
		return posY;
	}
	
	int[] getAdjacentRooms(){
		return adjacentRooms;
	}
	String room;
	String getAdjacentRoomSTR(int a) {
		
		for(int i=0; i<a; i++) {
			if(a == 0) {
				room ="George Allen Hall";
				r[0]=new Room("George Allen Field", 20, 20, new int[]{1,2,3});
			}
			else if (a==1) {
				room = "Japanese Garden";
			
			}
			
			else if (a==2) {
				room = "Student Parking";
				
			}
			else if (a==3) {
				room = "The Pyramid";
				
			}
			else if (a==4) {
				room = "West Walkway";
			
			}
			else if (a==5) {
				room = "Health Center";
				
			}
			else if (a==6) {
				room = "Forbidden Parking";
			
			}
			else if (a==7) {
				room = "Library";
			
			}
			else if (a==8) {
				room = "Union";
			
			}
			else if (a==9) {
				room = "Bratwurst Hall";
				
			}
			else if (a==10) {
				room = "East Walkway";
				
			}
			else if (a==11) {
				room = "Lab";
			
			}
			else if (a==12) {
				room = "North Hall";
			
			}
			else if (a==13) {
				room = "Room of Retirement";
				
			}
			else if (a==14) {
				room = "302";
				
			}
			else if (a==15) {
				room = "South Hall";
				
			}
			else if (a==16) {
				room = "Elevators";
				
			}
			else if (a==17) {
				room = "308";
				
			}
			else if (a==18) {
				room = "EAT";
				
			}
			else if (a==19) {
				room = "Conference";

			}
			else{
				room = "Noisy Room";

			}
		
			
			
			return room;
		}
		return room;
	}
	
	Room getRoomFromSTRING(String S ) {
			if(S.contains("George Allen Hall")) {
				r[0]=new Room("George Allen Field", 20, 20, new int[]{1,2,3});
				x=0;
			}
			else if(S.contains("Japanese Garden")) {
				room = "Japanese Garden";
				r[1]= new Room("Japanese Garden", 430, 20, new int[]{0,3,2});
				x=1;
			}
			
			else if(S.contains("Student Parking")) {
				room = "Student Parking";
				r[2] = new Room("Student Parking", 920, 20, new int[]{1,3,5,6});
				x=2;
			}
			else if(S.contains("The Pyramid")) {
				room = "The Pyramid";
				r[3]=	 new Room("The Pyramid", 430, 300, new int[]{0,1,2,5}); x=3;
			}
			else if(S.contains("West Walkway")) {
				room = "West Walkway";
				r[4]=new Room("West Walkway", 20, 670, new int[]{0,5,12,7}); x=4;
			}
			else if(S.contains("Health Center")) {
				room = "Health Center";
				r[5]=new Room("Health Center", 450, 540, new int[]{0,3,4,2,6}); x=5;
			}
			else if(S.contains("Health Center")) {
				room = "Forbidden Parking";
				r[6] =new Room("Forbidden Parking", 1020, 520, new int[]{2,5,10}); x=6;
			}
			else if(S.contains("Library")) {
				room = "Library";
				r[7] = new Room("Library", 20, 1540, new int[]{4,8}); x=7;
			}
			else if(S.contains("Union")) {
				room = "Union";
				r[8] = new Room("Union", 480, 1640, new int[]{7,9,16}); x=8;
			}
			else if(S.contains("Bratwurst Hall")) {
				room = "Bratwurst Hall";
				r[9] = new Room("Bratwurst Hall", 1050, 1660, new int[]{7,10}); x=9;
			}
			else if(S.contains("East Walkway")) {
				room = "East Walkway";
				r[10] = new Room("East Walkway", 1480, 960, new int[]{9,6,15}); x=10;
			}
			else if(S.contains("Lab")) {
				room = "Lab";
				r[11] = new Room("Lab", 180, 890, new int[]{12}); x=11;
				return r[11];
			}
			else if(S.contains("North Hall")) {
				room = "North Hall";
				r[12] = new Room("North Hall", 180, 1170, new int[]{4,11,14,13,16,15}); x=12;
			}
			else if(S.contains("Room of Retirement")) {
				room = "Room of Retirement";
				r[13] =  new Room("Room of Retirement", 180, 1360, new int[]{12}); x=13;
			}
			else if(S.contains("302")) {
				room = "302";
				r[14] =new Room("302", 620, 890, new int[]{12,15}); x=14;
			} 
			else if(S.contains("South Hall")) {
				room = "South Hall";
				r[15] = new Room("South Hall", 830, 1170, new int[]{14,18,19,17,20,10}); x=15;
			}
			else if(S.contains("Elevators")) {
				room = "Elevators";
				r[16]= new Room("Elevators", 620, 1360, new int[]{12,8}); x=16;
				return r[16];
			}
			else if(S.contains("308")) {
				room = "308";
				r[17]=new Room("308", 830, 1360, new int[]{15}); x=17;
			}
			else if(S.contains("EAT")) {
				room = "EAT";
				r[18]=new Room("EAT", 1050, 890, new int[]{15}); x=18;
			} 
			else if(S.contains("Conference")) {
				room = "Conference";
				r[19]=new Room("Conference", 1260, 890, new int[]{15}); x=19;
			}
			else{
				room = "Noisy Room";
				r[20]=new Room("Noisy Room", 1230, 1360, new int[]{15}); x=20;
			}
			return r[x];
		
	}
}
