import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

//CHOICE: 1=Learning Chip   2=Integrity Chip   3=Craft Chip     4=Quality Chip
public class Card3 extends Card{
	private ImageInputStream cardInput;
	public void play(Player p){
		if(p.getCurrentRoom()==7) {
			//Get one learning pt or one integrity pt
			p.setLearningPt(p.getLearnPt() +1);
		}
	}
	
	public String getName() {
		return ("Math 122");
	}
	
	public ImageInputStream displayPIcture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Randy/Desktop/Cards/Card3.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("I ugly");
		}
		return cardInput;
	}}
