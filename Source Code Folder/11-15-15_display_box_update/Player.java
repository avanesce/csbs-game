/* CLASS PLAYER
 * Function: Individual player object that contains
 * 	all values that determine a player's validity
 *  in movement and card play.
 */
import java.util.*;

public class Player {
	private final int DEFAULT_MOVE = 3;
	private final int DEFAULT_ROOM = 1;
	private String playerName;
	private int currentRoom;
	private String currentCard;
	private int learnPt;
	private int craftPt;
	private int integPt;
	private int qualPt;
	int skillChips;
	private int moveCount = 3;
	List<Card> hand = new ArrayList<Card>();
	Card currentCardHand;

	
	//Default Constructor
	public Player(){}
	//Constructor
	public Player(String newName){
		playerName = newName;
	}
	/************************************************/
	/***********Standard GET/SET FUCNTIONS***********/
	/************************************************/
	String getPlayerName(){
		return playerName;
	}
	
	void setCurrentCardHand(Card c) {
		currentCardHand = c;
	}
	
	Card getCurrentCardHand() {
		return currentCardHand;
	}
	
	void setPlayerName(String newPlayerName){
		playerName = newPlayerName;
	}
	
	int getCurrentRoom(){
		return currentRoom;
	}
	
	void setCurrentRoom(int newCurrentRoom){
		currentRoom = newCurrentRoom;
	}
	
	void setCurrentCard(String card) {
		currentCard = card;
	}
	
	String getCurrentCard() {
		return currentCard;
	}
	
	int getLearnPt(){
		return learnPt;
	}
	
	int getCraftPt(){
		return craftPt;
	}
	
	int getIntegPt(){
		return integPt;
	}
	
	int getQlityPt(){
		return qualPt;
	}
	
	//Function updates all points at once
	void setAllPoints(int[] addPts){
		learnPt += addPts[0];
		craftPt += addPts[1];
		integPt += addPts[2];
	}
	
	int getSkillChips(){
		return skillChips;
	}
	
	void setSkillChips(int newSkillChips){
		skillChips = newSkillChips;
	}
	
	
	void setLearningPt(int lp){
		learnPt = lp;
	}
	
	void setCraftPt(int cp){
		craftPt = cp;
	}
	
	void setIntegrityPt(int ip) {
		integPt = ip;
	}
	/************************************************/
	/************************************************/
	
	
	void setPlayerHand(Card c) {
		
	}
	void resetMoveCount(){
		moveCount = DEFAULT_MOVE;
	}
	
	int getMoveCount(){
		return moveCount;
	}
	
	void decrMoveCount(){
		moveCount--;
	}
	
	//Insert random card from deck into hand
	//i increments and decrements to bypass Random exception
	void drawCard(List<Card> deck){
		Random rand = new Random();
		int i = rand.nextInt(deck.size() + 1);
		hand.add(deck.get(i-1));
	}
	
	//Remove card in hand of index i
	void discardCard(int i){
		hand.remove(i);
	}
	
}

