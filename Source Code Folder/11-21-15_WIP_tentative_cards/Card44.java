import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;

public class Card44 extends Card{
	private ImageInputStream cardInput;
	
	public void play(Player p, String c){
		if(p.getCurrentRoom()==11) {
			p.setLearningPt(p.getLearnPt() + 4);
		}
	}
	
	public String getName() {
		return ("CECS 285");
	}
	
	public ImageInputStream displayPIcture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Randy/Desktop/Cards/Card44.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("I ugly");
		}
		return cardInput;
	}}