import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;

public class Card43 extends Card{
	private ImageInputStream cardInput;
	
	public void play(Player p, String c){
		if(p.getCurrentRoom()==17) {
			p.setLearningPt(p.getLearnPt() + 1);
		}
	}
	
	public String getName() {
		return ("CECS 228");
	}
	
	public ImageInputStream displayPIcture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Randy/Desktop/Cards/Card43.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("I ugly");
		}
		return cardInput;
	}}