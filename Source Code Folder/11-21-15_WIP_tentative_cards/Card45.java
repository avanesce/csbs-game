import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;

public class Card45 extends Card{
	private ImageInputStream cardInput;
	
	public void play(Player p, String c){
		if(p.getCurrentRoom()==18) {
			p.setLearningPt(p.getLearnPt() + 1);
		}
	}
	
	public String getName() {
		return ("CECS 282");
	}
	
	public ImageInputStream displayPIcture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Randy/Desktop/Cards/Card45.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("I ugly");
		}
		return cardInput;
	}}