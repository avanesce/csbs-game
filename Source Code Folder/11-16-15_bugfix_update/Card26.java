import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card26 extends Card{
	String cardName = "Loud Buzzing"; 
	int roomReq = 18;
	int yearReq = 1;
	int[] reqPoints = {0,3,0,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p, String input){
		if(p.getCurrentRoom()==roomReq && p.getCraft()>=reqPoints[1]){
			//ADD POINTS
			//GET CHIP OF CHOICE
			if(input.equals("Get 1 Learning Chip")) {
				p.setLearn(1);
				//PRINT SUCCESS
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						 + cardName + " for 1 Learning Chip.\n" );
				}
				//LEARNING
				else if(input.equals("Get 1 Learning Chip")) {
				p.setInteg(1);
				//PRINT SUCCESS
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						 + cardName + " for 1 Integrity Chip.\n" );
				}
				else if(input.equals("Get 1 Craft Chip")) {
					p.setCraft(1);
					//PRINT SUCCESS
					Board.currentPlayText.append(p.getPlayerName() + " has played " 
							 + cardName + " for 1 Craft Chip.\n" );
					}
			//PRINT SUCCESS
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 1 selected chip.\n");
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n" );
		}
	}
	
	public String getName() {
		return ("Loud Buzzing");
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card26.png");
			cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("I ugly");
		}
		return cardInput;
	}
}