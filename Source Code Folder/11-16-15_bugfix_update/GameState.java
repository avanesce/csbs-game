/* CLASS GAMESTATE
 * Function: Runs main method.
 *  Keeps track of global game values:
 *  	RoomList
 *  	Deck
 *  	Discard Pile
 *  	Current Game Year
 *  	Total Quality Points of all players
 */
import java.io.IOException;
import java.util.*;

public class GameState {
	public static Player[] playerList = new Player[3];
	public static Room[] roomList;
	public static int currentPlayer = 0;
	public static Room currentPlayerRoom;
	//Used for implementation later
	static List<Card> deck = new ArrayList<Card>();
	static ArrayList discardPile = new ArrayList();
	int year = 1;
	int totalQP = 0; 
	public static Board board;
	
	//Main Functions
	public static void main(String[] args) throws IOException{
		/**************INITIAL GAME SETUP***************/
		initialSetup();
		/**************TESTING SECTION***************/
		
		/**************MAIN GAME LOOP***************/
		
		//GAME LOOP - until a player reaches 100 QP
		while(playerList[0].getQual()<100 ||
		      playerList[1].getQual()<100 ||
		      playerList[2].getQual()<100){
			
			//Player Turn
			if(currentPlayer == 0){
				//playerTurn(playerList[currentPlayer]);
			}
			//AI Turn
			else{
				aiTurn(playerList[currentPlayer]);
			}
			//Check round reset
			if(currentPlayer==3)
				currentPlayer = 0;
		}
		
		if(playerList[0].getQual()<100){
			//output to CurrentPlayPanel player 1 has won
		}
		else if(playerList[0].getQual()<100){
			//output to CurrentPlayPanel player 2 has won
		}
		else{
			//output to CurrentPlayPanel player 3 has won
		}
		
	}
	/**************GAME PLAY FUNCTIONS***************/
	/* Function provides overall turn for human player
	 * Player chooses room and card to play
	 */
	static void playerTurn(Player player){
		//WHEN BUTTON IS PRESSED player.drawCard(deck);
		player.resetMoveCount();
		while(player.getMoveCount() > 0){
			//LISTEN FOR MOVE
			player.decrMoveCount();
			//IF PLAY CARD, BREAK OUT OF WHILE LOOP
			//currentPlayer++; //AI PLAYERS WILL NOT MOVE IF THIS IS ACTIVATED
								  //REMOVE THIS IN FINAL BUILD, ONLY FOR TESTING
		}
		//playCard();
		
		while(player.hand.size() > 7){
			//DETERMINE WHAT CARD TO DISCARD
			player.discardCard(0);
		}
		
		//GIVE CHIP IF QP PASSES 15/30/45/60/75/90
		
		//DO NOT ACTIVATE UNTIL GAME WAITS FOR TURN TO FINISH/currentPlayer++;
	}
	
	/* Function provides overall turn for AI player
	 * AI moves randomly to adjacent rooms and plays a card
	 */
	static void aiTurn(Player player){
		player.resetMoveCount();
		Random rand = new Random();
		int i;
		while(player.getMoveCount() > 0){
			i = rand.nextInt(roomList[player.getCurrentRoom()].adjacentRooms.length);
			player.setCurrentRoom(roomList[player.getCurrentRoom()].adjacentRooms[i]);
			Board.redrawPlayer(roomList[player.getCurrentRoom()], currentPlayer);
			board.currentPlayText.append(player.getPlayerName() + " has moved to " 
					 + GameState.roomList[player.getCurrentRoom()].getRoomName() + "\n");
			player.decrMoveCount();
		}
		playCardAI();
		//GIVE CHIP IF QP PASSES 15/30/45/60/75/90
		currentPlayer++;
	}
	
	/* Function allows AI player to play a card
	 * 
	 */
	static void playCardAI(){
		//search deck for appropriate card
		//use card -> assign values
		//move card DIRECTLY from deck to discard pile 
	}
	
	/**************INITIAL SETUP FUNCTIONS**************/
	/* Function INITIAL SETUP
	 * Creates the game window (board+ui)
	 * Creates the players and rooms
	 * Loads player labels onto the board
	 */
	
	static void initialSetup() throws IOException{
		board = new Board();
		createPlayers();
		createRooms();	
		createDeck();
		
		playerList[0].drawCard(deck);
		playerList[0].drawCard(deck);
		playerList[0].drawCard(deck);
		playerList[0].drawCard(deck);
		playerList[0].drawCard(deck);
		playerList[0].drawCard(deck);
		
		System.out.println(playerList[0].hand.get(0).getName());
		System.out.println(playerList[0].hand.get(1).getName());
		System.out.println(playerList[0].hand.get(2).getName());
		System.out.println(playerList[0].hand.get(3).getName());
		System.out.println(playerList[0].hand.get(4).getName());
		System.out.println(playerList[0].hand.get(5).getName());
		
		//Initial Player Tokens and Window Board setup
		for (int i = 0; i < 3; i++){
			board.createPlayerLabel(playerList[i].getPlayerName(),i);
			playerList[i].setCurrentRoom(17);
			board.redrawPlayer(roomList[playerList[i].getCurrentRoom()], i);
		}
		board.createMoveList(roomList[playerList[currentPlayer].getCurrentRoom()], roomList);
		board.createAndDrawGUI();
	}
	
	/* Function creates the INITIAL array of players
	 * Players are assigned names and points randomly
	 */
	static void createPlayers(){
		Random r = new Random();
		String[] names = {"Matt", "Kevin", "Bob"};
		int n = r.nextInt(3);
		int[][] defaultPts = {
			{2, 2, 2},
			{3, 1, 2},
			{0, 3, 3}
		};

		for(int i = 0; i < 3; i++){
			playerList[i] = new Player(names[n]);
			playerList[i].setAllPoints(defaultPts[i]);
			n++;
			if(n == 3)
				n = 0;
		}
	}
	
	/* Function creates the INITIAL array of rooms
	 * Room values are all hard-coded.
	 */
	static void createRooms(){
		Room[] rooms = {
		 new Room("George Allen Field", 20, 20, new int[]{1,5,3,4}), //0
	 	 new Room("Japanese Garden", 430, 20, new int[]{0,3,2}), //1
		 new Room("Student Parking", 920, 20, new int[]{1,3,5,6}), //2
		 new Room("The Pyramid", 430, 300, new int[]{0,1,2,5}), //3
		 new Room("West Walkway", 20, 670, new int[]{0,5,12,7}), //4
		 new Room("Rec Center", 450, 540, new int[]{0,3,4,2,6}), //5
		 new Room("Forbidden Parking", 1020, 520, new int[]{2,5,10}), //6
		 new Room("Library", 20, 1540, new int[]{4,8}), //7
		 new Room("LA5", 480, 1640, new int[]{7,9,16}), //8
		 new Room("Bratwurst Hall", 1050, 1660, new int[]{8,10}), //9
		 new Room("East Walkway", 1480, 960, new int[]{9,6,15}), //10
		 new Room("Lab", 180, 890, new int[]{12}), //11
		 new Room("North Hall", 180, 1170, new int[]{4,11,14,13,16,15}), //12
		 new Room("Room of Retirement", 180, 1360, new int[]{12}), //13 
		 new Room("302", 620, 890, new int[]{12,15}), //14
		 new Room("South Hall", 830, 1170, new int[]{12,14,18,19,17,20,10}), //15
		 new Room("Elevators", 620, 1360, new int[]{12,8}), //16
		 new Room("308", 830, 1360, new int[]{15}), //17
		 new Room("EAT", 1050, 890, new int[]{15}), //18
		 new Room("Conference", 1260, 890, new int[]{15}), //19
		 new Room("Lactation Lounge", 1230, 1360, new int[]{15}) //20
		};
		roomList = rooms;
	}
	
	/* Function creates the INITIAL array of cards
	 * Cards are pulled from their individual classes
	 * Deck is shuffled once created
	 */
	static void createDeck(){
		deck.add(new Card1());
		deck.add(new Card2());
		deck.add(new Card3());
		deck.add(new Card4());
		deck.add(new Card5());
		deck.add(new Card6());
		deck.add(new Card7());
		deck.add(new Card8());
		deck.add(new Card9());
		/*deck.add(new Card10());
		deck.add(new Card11());
		deck.add(new Card12());
		deck.add(new Card13());
		deck.add(new Card14());
		deck.add(new Card15());
		deck.add(new Card16());
		deck.add(new Card17());
		deck.add(new Card18());
		deck.add(new Card19());
		deck.add(new Card20());
		deck.add(new Card21());
		deck.add(new Card22());
		deck.add(new Card23());
		deck.add(new Card24());
		deck.add(new Card25());
		deck.add(new Card26());
		deck.add(new Card27());
		deck.add(new Card28());
		deck.add(new Card29());
		deck.add(new Card30());
		deck.add(new Card31());
		deck.add(new Card32());
		deck.add(new Card33());
		deck.add(new Card34());
		deck.add(new Card35());
		deck.add(new Card36());
		deck.add(new Card37());
		deck.add(new Card38());
		deck.add(new Card39());
		*/
		shuffleDeck();
	}
	
	/* Function moves discard pile into deck
	 * Calls shuffle afterwards
	 */
	static void reshuffleDeck(){
		//Shuffle Discard Pile into Deck
		shuffleDeck();
	}
	
	//Function shuffles the deck when needed
	static void shuffleDeck(){
		long seed = System.nanoTime();
		Collections.shuffle(deck, new Random(seed));
	}
}
