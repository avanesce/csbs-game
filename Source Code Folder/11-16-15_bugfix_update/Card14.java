import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card14 extends Card{
	String cardName = "Physics 151"; 
	int roomReq = 17;
	int yearReq = 1;
	int[] reqPoints = {0,3,0,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p, String s){
		if(p.getCurrentRoom()==roomReq && p.getCraft()>=reqPoints[1]){
			//ADD POINTS
			p.setQual(5);
			//PRINT SUCCESS
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 5 Quality Points.\n");
		}
		else{
			//TAKE POINTS
			p.setQual(-5);
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 5 Quality Points.\n" );
		}
	}
	
	public String getName() {
		return ("Physics 151");
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card14.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("I ugly");
		}
		return cardInput;
	}

}
