import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card31 extends Card{
	String cardName = "Program Crashes"; 
	int roomReq = 7;
	int yearReq = 1;
	int[] reqPoints = {2,0,0,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p, String s){
		if(p.getCurrentRoom()==roomReq && p.getLearn()>=reqPoints[0]){
			//ADD POINTS
			p.setLearn(1);
			//DRAW CARD
			//PRINT SUCCESS
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 5 Quality Points.\n" +
					 "Player gains an additional card.\n");
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n");
		}
	}
	public String getName() {
		return ("Elective Class");
	}
	
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card31.png");
			cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("I ugly");
		}
		return cardInput;
	}
}