import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card1 extends Card{
	String cardName = "Make a friend"; 
	int roomReq = 14;
	int roomReq2 = 17;
	int yearReq = 1;
	int[] reqPoints = {0,0,2,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p, String input){
		if(p.getCurrentRoom()==roomReq || p.getCurrentRoom()==roomReq2){
			//ADD POINTS
			p.setLearn(1);
			//PRINT SUCCESS
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 1 Learning Chip.\n" );
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n" );
		}
	}
	
	public String getName() {
		return ("CECS 105");
	}

	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Randy/Desktop/Cards/Card1.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("I ugly");
		}
		return cardInput;
	}

}
