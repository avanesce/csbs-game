/* CLASS ROOM
 * Function: Room contains final values for game
 *  objects that relate to the position of rooms.
 *  Values are used by players, card, and the board
 *  for determining movement and position.
 */
public class Room {
	String roomName;
	int posX;
	int posY;
	int[] adjacentRooms;
	
	//Default Constructor
	public Room(){	
	}
	
	//Constructor with value assignment
	public Room(String newRoomName, int newPosX, int newPosY, int[] adjRoom){
		roomName = newRoomName;
		posX = newPosX;
		posY = newPosY;
		adjacentRooms = adjRoom;
	}

	/***********Standard GET/SET FUCNTIONS***********/
	//SET functions are not included as all room values are not to be modified
	String getRoomName(){
		return roomName;
	}
	
	int getPosX(){
		return posX;
	}
	
	int getPosY(){
		return posY;
	}
	
	int[] getAdjacentRooms(){
		return adjacentRooms;
	}
}
