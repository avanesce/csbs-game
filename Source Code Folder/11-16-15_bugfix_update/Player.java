/* CLASS PLAYER
 * Function: Individual player object that contains
 * 	all values that determine a player's validity
 *  in movement and card play.
 */
import java.util.*;

public class Player {
	private final int DEFAULT_MOVE = 3;
	private final int DEFAULT_ROOM = 1;
	private String playerName;
	private int currentRoom;
	private int learnPt;
	private int craftPt;
	private int integPt;
	private int qualPt;
	int skillChips;
	private int moveCount = 3;
	private boolean drawTF;
	private boolean playTF;
	private boolean moveTF;
	List<Card> hand = new ArrayList<Card>();
	Card currentCard;
	
	//Default Constructor
	public Player(){}
	//Constructor
	public Player(String newName){
		playerName = newName;
	}
	/************************************************/
	/***********Standard GET/SET FUCNTIONS***********/
	/************************************************/
	
	boolean getDrawTF() {
		return drawTF;
	}
	
	void setDrawTF(boolean t) {
		drawTF = t;
	}
	
	void setCurrentCardHand(Card c) {
		currentCard = c;
	}
	
	Card getCurrentCardHand() {
		return currentCard;
	}
	
	boolean getMoveTF() {
		return moveTF;
	}
	
	void setMoveTF(boolean t) {
		moveTF = t;
	}
	
	boolean getPlayTF() {
		return playTF;
	}
	
	void setPlayTF(boolean t) {
		playTF = t;
	}
	
	
	String getPlayerName(){
		return playerName;
	}
	
	void setPlayerName(String newPlayerName){
		playerName = newPlayerName;
	}
	
	int getCurrentRoom(){
		return currentRoom;
	}
	
	void setCurrentRoom(int newCurrentRoom){
		currentRoom = newCurrentRoom;
	}
	
	int getLearn(){
		return learnPt;
	}
	
	void setLearn(int newPt){
		learnPt += newPt;
	}
	
	int getCraft(){
		return craftPt;
	}
	
	void setCraft(int newPt){
		craftPt += newPt;
	}
	
	int getInteg(){
		return integPt;
	}
	
	void setInteg(int newPt){
		integPt += newPt;
	}
	
	int getQual(){
		return qualPt;
	}
	
	void setQual(int newPt){
		qualPt += newPt;
	}
	
	//Function updates all points at once
	void setAllPoints(int[] addPts){
		learnPt += addPts[0];
		craftPt += addPts[1];
		integPt += addPts[2];
	}
	
	int getSkillChips(){
		return skillChips;
	}
	
	void setSkillChips(int newSkillChips){
		skillChips = newSkillChips;
	}
	
	/************************************************/
	/************************************************/
	
	void resetMoveCount(){
		moveCount = DEFAULT_MOVE;
	}
	
	int getMoveCount(){
		return moveCount;
	}
	
	void decrMoveCount(){
		moveCount--;
	}
	
	//Insert random card from deck into hand
	void drawCard(List<Card> deck){
		Random rand = new Random();
		int i = rand.nextInt(deck.size());
		hand.add((Card)deck.get(i));
	}
	
	//Remove card in hand of index i
	void discardCard(int i){
		hand.remove(i);
	}
	
}

