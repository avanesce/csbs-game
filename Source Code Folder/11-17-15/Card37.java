import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card37 extends Card{
	String cardName = "Make a friend"; 
	int roomReq = 12;
	int roomReq2 = 15;
	int yearReq = 1;
	int[] reqPoints = {0,0,2,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p, String input){
		if((p.getCurrentRoom() == roomReq || p.getCurrentRoom() == roomReq2)
				&& p.getInteg() >= reqPoints[2]){
			//ADDPOINTS
			p.setQual(7);
			//LEARNING
			if(input.equals("Get 1 Learning Chip")) {
				p.setLearn(1);
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 7 Quality Points and 1 Learning Chip.\n" );
			}
			//LEARNING
			else if(input.equals("Get 1 Learning Chip")) {
				p.setInteg(1);
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 7 Quality Points and 1 Integrity Chip.\n" );
			}
			//CRAFT
			else if(input.equals("Get 1 Craft Chip")) {
				p.setCraft(1);
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 7 Quality Points and 1 Craft Chip.\n" );
			}
		}
		else{
			p.setQual(-2);
			//Player discard a card
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points and must discard a card.\n" );
		}
	}
	
	public String getName() {
		return ("Make a Friend");
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card37.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("Card37.png does not exist.");
		}
		return cardInput;
	}

}
