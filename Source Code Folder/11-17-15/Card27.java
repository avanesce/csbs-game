import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card27 extends Card{
	String cardName = "Program Crashes"; 
	int roomReq = 27;
	int yearReq = 1;
	int[] reqPoints = {2,0,0,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p, String input){
		if(p.getCurrentRoom()==roomReq && p.getLearn()>=reqPoints[0]){
			//ADD POINTS
			p.setQual(5);
			//CHOICE OF CHIP
			
			if(input.equals("Get 1 Learning Chip")) {
				p.setLearn(1);
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 1 Learning Chip.\n" );
				}
			//INTEGRITY
			else if(input.equals("Get 1 Integrity Chip")) {
				p.setInteg(1);
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 1 Integrity Chip.\n" );
			}
			//CRAFT
			else if(input.equals("Get 1 Craft Chip")) {
				p.setCraft(1);
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 1 Craft Chip.\n" );
			}
			//PRINT SUCCESS
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 5 Quality Points and 1 selected chip.\n");
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//DISCARD CARD
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n" +
					 "Player must discard a card.\n");
		}
	}
	
	
	public String getName() {
		return ("Program Crashes");
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card27.png");
			cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("Card27.png does not exist.");
		}
		return cardInput;
	}
}