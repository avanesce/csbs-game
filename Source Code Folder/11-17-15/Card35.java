import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card35 extends Card{
	String cardName = "Program Crashes"; 
	int roomReq = 6;
	int roomReq2 = 11;
	int yearReq = 1;
	int[] reqPoints = {0,0,0,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p, String input){
		if(p.getCurrentRoom()==roomReq && p.getCurrentRoom()<roomReq2){
			//CHIP OF CHOICE
			//LEARNING
			if(input.equals("Get 1 Learning Chip")) {
				p.setLearn(1);
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						 + cardName + " for 1 Learning Chip.\n" );
			}
			//INTEGRITY
			else if(input.equals("Get 1 Learning Chip")) {
				p.setInteg(1);
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						 + cardName + " for 1 Integrity Chip.\n" );
			}
			//CRAFT
			else if(input.equals("Get 1 Craft Chip")) {
				p.setCraft(1);
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						 + cardName + " for 1 Craft Chip.\n" );
			}
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n");
		}
	}
	
	public String getName() {
		return ("The Outpost");
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card35.png");
			cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("Card35.png does not exist.");
		}
		return cardInput;
	}
}