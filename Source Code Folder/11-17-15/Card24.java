import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card24 extends Card{
	String cardName = "A New Laptop"; 
	int roomReq = 11;
	int yearReq = 1;
	int[] reqPoints = {0,0,4,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p, String input){
		if(p.getCurrentRoom()==roomReq && p.getInteg()>=reqPoints[2]){
			//ADD POINTS
			p.setQual(3);
			//ADD CHIP OF CHOICE
			//PRINT SUCCESS
			if(input.equals("Get 1 Learning Chip")) {
				p.setLearn(1);
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 1 Learning Chip.\n" );
				}
			//INTEGRITY
			else if(input.equals("Get 1 Integrity Chip")) {
				p.setInteg(1);
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 1 Integrity Chip.\n" );
			}
			//CRAFT
			else if(input.equals("Get 1 Craft Chip")) {
				p.setCraft(1);
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 1 Craft Chip.\n" );
			}
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 3 Quality Points and a selected chip.\n");
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//DISCARD A CARD
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n" +
					 "Player must discard a card\n");
		}
	}
	
	
	public String getName() {
		return ("A New Laptop");
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card24.png");
			cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("Card24.png does not exist.");
		}
		return cardInput;
	}
}