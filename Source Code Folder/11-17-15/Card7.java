import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card7 extends Card{
	String cardName = "Student Parking"; 
	int roomReq = 5;
	int yearReq = 1;
	int[] reqPoints = {0,0,0,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p, String input){
		if(p.getCurrentRoom()==roomReq){
			//ADD POINTS
			p.setInteg(1);
			//PRINT SUCCESS
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 1 Integrity Chip.\n");
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n" );
		}
	}
	
	public String getName() {
		return ("Exercising Mind and Body");
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card7.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("Card7.png does not exist.");
		}
		return cardInput;
	}

}
