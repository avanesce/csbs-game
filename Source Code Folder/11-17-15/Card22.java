import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card22 extends Card{
	String cardName = "Fall in the Pond"; 
	int roomReq = 1;
	int yearReq = 1;
	int[] reqPoints = {3,0,0,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p, String s){
		if(p.getCurrentRoom()==roomReq && p.getLearn()>=reqPoints[0]){
			//ADD POINTS
			p.setInteg(1);
			p.setCraft(1);
			//PRINT SUCCESS
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 1 Craft Chip and 1 Integrity Chip.\n");
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//MOVE PLAYER
			p.setCurrentRoom(20);
			GameState.board.redrawPlayer(GameState.roomList[20], GameState.currentPlayer);
			GameState.board.redrawMoveList(GameState.roomList[20], GameState.roomList);
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n" +
					 "Played is moved to the Lactation Lounge.\n");
		}
	}
	
	public String getName() {
		return ("Fall in the Pond");
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card22.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("Card22.png does not exist.");
		}
		return cardInput;
	}

}
