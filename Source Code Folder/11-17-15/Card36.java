import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card36 extends Card{
	String cardName = "Learning Linux"; 
	int roomReq = 11;
	int yearReq = 1;
	int[] reqPoints = {0,2,3,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p, String input){
		if(p.getCurrentRoom() == roomReq
				&& p.getCraft() >= reqPoints[1]
				&& p.getInteg() >= reqPoints[2]){
			//ADDPOINTS
			p.setQual(7);
			//PROMPT FOR CHIP OF CHOICE
			//LEARNING
			if(input.equals("Get 1 Learning Chip")) {
				p.setLearn(1);
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						 + cardName + " for 1 Learning Chip.\n" );
			}
			//INTEGRITY
			else if(input.equals("Get 1 Integrity Chip")) {
				p.setInteg(1);
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						 + cardName + " for 1 Integrity Chip.\n" );
			}
			//CRAFT
			else if(input.equals("Get 1 Craft Chip")) {
				p.setCraft(1);
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						 + cardName + " for 1 Craft Chip.\n" );
			}
			
			
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 7 Quality Points and a selected chip.\n" );
		}
		else{
			p.setQual(-3);
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 3 Quality Points.\n" );
		}
	}
	
	public String getName() {
		return ("Learning Linux");
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card36.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("Card36.png does not exist.");
		}
		return cardInput;
	}

}
