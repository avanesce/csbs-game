import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card33 extends Card{
	String cardName = "Professor Hoffman"; 
	int roomReq = 11;
	int roomReq2 = 20;
	int yearReq = 1;
	int[] reqPoints = {3,0,0,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p, String input){
		if(p.getCurrentRoom()>=roomReq 
			&& p.getCurrentRoom()!=roomReq2
			&& p.getLearn()>=reqPoints[0]){
			//ADD POINTS
			p.setQual(5);
			//DRAW TWICE
			p.drawCard(GameState.deck);
			p.drawCard(GameState.deck);
			//PRINT SUCCESS
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 5 Quality Points.\n" +
					 "Player gains two additional cards.\n");
		}
		else{
			//TAKE POINTS
			p.setQual(-7);
			//MOVE PLAYER
			p.setCurrentRoom(20);
			GameState.board.redrawPlayer(GameState.roomList[20], GameState.currentPlayer);
			GameState.board.redrawMoveList(GameState.roomList[20], GameState.roomList);
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 7 Quality Points.\n" + 
					 "Player is moved to the Lactation Lounge.\n");
		}
	}
	
	public String getName() {
		return ("Professor Hoffman");
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card33.png");
			cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("Card33.png does not exist.");
		}
		return cardInput;
	}
}