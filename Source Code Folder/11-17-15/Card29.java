import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card29 extends Card{
	String cardName = "Press the Right Floor"; 
	int roomReq = 16;
	int yearReq = 1;
	int[] reqPoints = {4,0,0,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p , String s){
		if(p.getCurrentRoom()==roomReq && p.getLearn()>=reqPoints[0]){
			//ADD POINTS
			p.setCraft(2);
			//PRINT SUCCESS
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 2 Craft Chips.\n");
		}
		else{
			//TAKE POINTS
			p.setQual(-4);
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 4 Quality Points.\n");
		}
	}
	
	public String getName() {
		return ("Press the Right Floor");
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Andy/workspace/343CSBS/src/ImageAssets/Card29.png");
			cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("Card29.png does not exist.");
		}
		return cardInput;
	}
}