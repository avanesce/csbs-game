import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import javax.swing.JOptionPane;


public class Card17 extends Card{
	String cardName = "Math 123"; 
	int roomReq = 14;
	int roomReq2 = 17;
	int yearReq = 1;
	int[] reqPoints = {5,0,0,0};
	CardChooser c;
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p, String s) throws IOException{
		c = new CardChooser(p);
		if((p.getCurrentRoom()==roomReq || p.getCurrentRoom()==roomReq2)
			&& p.getLearn()>=reqPoints[0]){
			//ADD POINTS
			p.setQual(9);
			//PRINT SUCCESS
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 9 Quality Points.\n");
		}
		else{
			//TAKE POINTS
			p.setQual(-5);
			//DISCARD A CARD TO DISCARD PILE
			String[] choices = {p.hand.get(0).getName(), p.hand.get(1).getName(), p.hand.get(2).getName(),
				  p.hand.get(3).getName(), p.hand.get(4).getName()};

			c.displayCardChooser();
			
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 5 Quality Points.\n" +
					 "Player must also discard a card.\n");
		}
	}

	public String getName() {
		return ("Math 123");
	}
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Randy/Desktop/Cards/Card17.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("I ugly");
		}
		return cardInput;
	}

}
