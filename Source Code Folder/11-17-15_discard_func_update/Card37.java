import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import javax.swing.JOptionPane;


public class Card37 extends Card{
	String cardName = "Make a friend"; 
	int roomReq = 12;
	int roomReq2 = 15;
	int yearReq = 1;
	int[] reqPoints = {0,0,2,0};
	Board b;
	CardChooser c;
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p, String input) throws IOException{
		c = new CardChooser(p);
		if((p.getCurrentRoom() == roomReq || p.getCurrentRoom() == roomReq2)
				&& p.getInteg() >= reqPoints[2]){
			p.setQual(7);
			//Insert popup for choose a chip
			
			if(input.equals("Get 1 Learning Chip")) {
				p.setLearn(1);
				//PRINT SUCCESS
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						 + cardName + " for 1 Learning Chip.\n" );
				}
				//LEARNING
				else if(input.equals("Get 1 Learning Chip")) {
				p.setInteg(1);
				//PRINT SUCCESS
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						 + cardName + " for 1 Integrity Chip.\n" );
				}
				else if(input.equals("Get 1 Craft Chip")) {
					p.setCraft(1);
					//PRINT SUCCESS
					Board.currentPlayText.append(p.getPlayerName() + " has played " 
							 + cardName + " for 1 Craft Chip.\n" );
					}
			
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 7 Quality Points and a selected skill chip.\n" );
		}
		else{
			p.setQual(-2);
			//Player discard a card
			
			c.displayCardChooser();
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points and must discard a card.\n" );
		}
	}
	
	public String getName() {
		return ("Make a Friend");
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Randy/Desktop/Cards/Card37.png");
	   cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("I ugly");
		}
		return cardInput;
	}

}
