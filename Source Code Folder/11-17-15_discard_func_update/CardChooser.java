import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;


public class CardChooser {
	JFrame mFrame = new JFrame();
	JPanel cardPanel = new JPanel();
	JButton discardButton = new JButton();
	JLabel cardPicture = new JLabel();
	int imageCounter=0;
	
	public CardChooser(final Player p) throws IOException {
		cardPicture = new JLabel(new ImageIcon(ImageIO.read(p.hand.get(0).displayPicture())));
		discardButton = new JButton("Discard");
		cardPanel.add(cardPicture);
		cardPanel.add(discardButton);
		discardButton.setLocation(10, 10);
		
		//mFrame.add(discardButton);
		mFrame.add(cardPanel);
		
		Insets insets = cardPanel.getInsets();
		insets.top += 15;
		insets.left += 25;
		
		Dimension size = discardButton.getPreferredSize();
		discardButton.setBounds(insets.left, insets.top,
			size.width + 60, size.height);
		
		size = cardPicture.getPreferredSize();
		cardPicture.setBounds(insets.left, insets.top + 75,
			size.width + 94, size.height + 50);
		
		p.setCurrentCardHand(p.hand.get(0));
		cardPicture.addMouseListener(new MouseAdapter(){  
			public void mouseClicked(MouseEvent e){ 	   	
				try {  
					do{
						imageCounter++;
						if(imageCounter>p.hand.size()-1) {
							imageCounter = 0;
							cardPicture.setIcon(new ImageIcon(ImageIO.read(p.hand.get(imageCounter).displayPicture())));
							p.setCurrentCardHand(p.hand.get(imageCounter));
						}
						else{
							cardPicture.setIcon(new ImageIcon(ImageIO.read(p.hand.get(imageCounter).displayPicture())));
							p.setCurrentCardHand(p.hand.get(imageCounter));
						}
					}while(p.hand.equals(null));
			
					}
				catch (IOException e1) {
					e1.printStackTrace();
				}
	      }  
	  }); 
		
		discardButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				
				  if(p.hand.get(0).getName() == p.hand.get(imageCounter).getName() ) {
              	p.discardCard(0);
              }
              else  if(p.hand.get(1).getName() == p.hand.get(imageCounter).getName() ) {
              	p.discardCard(1);
              }
              else  if(p.hand.get(2).getName() == p.hand.get(imageCounter).getName() ) {
              	p.discardCard(2);
              }
              else  if(p.hand.get(3).getName() == p.hand.get(imageCounter).getName() ) {
              	p.discardCard(3);
              }
              else  if(p.hand.get(4).getName() == p.hand.get(imageCounter).getName() ) {
              	p.discardCard(4);
              }
              else  if(p.hand.get(5).getName() == p.hand.get(imageCounter).getName() ) {
                	p.discardCard(5);
                }
              else  if(p.hand.get(6).getName() == p.hand.get(imageCounter).getName() ) {
                	p.discardCard(6);
                }
				 
				  mFrame.dispose();
			}
			});
	}

	public JFrame displayCardChooser() {
		
		mFrame.setVisible(true);
		mFrame.setContentPane(cardPanel);
		mFrame.setVisible(true);
		mFrame.setResizable(true);
		mFrame.setSize(500,500);
		
		return mFrame;
	}
}

