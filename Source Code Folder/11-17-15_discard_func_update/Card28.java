import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;


public class Card28 extends Card{
	String cardName = "Professor Englert"; 
	int roomReq = 19;
	int yearReq = 1;
	CardChooser c;
	int[] reqPoints = {0,0,3,0};
	
	private ImageInputStream cardInput;
	
	@Override
	public void play(Player p, String input) throws IOException{
		c = new CardChooser(p);
		if(p.getCurrentRoom()==roomReq && p.getInteg()>=reqPoints[2]){
			//ADD POINTS
			//CHOICE OF CHIP
			
			if(input.equals("Get 1 Learning Chip")) {
				p.setLearn(1);
				//PRINT SUCCESS
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						 + cardName + " for 1 Learning Chip.\n" );
				}
				//LEARNING
				else if(input.equals("Get 1 Learning Chip")) {
				p.setInteg(1);
				//PRINT SUCCESS
				Board.currentPlayText.append(p.getPlayerName() + " has played " 
						 + cardName + " for 1 Integrity Chip.\n" );
				}
				else if(input.equals("Get 1 Craft Chip")) {
					p.setCraft(1);
					//PRINT SUCCESS
					Board.currentPlayText.append(p.getPlayerName() + " has played " 
							 + cardName + " for 1 Craft Chip.\n" );
					}
			//PRINT SUCCESS
			Board.currentPlayText.append(p.getPlayerName() + " has played " 
					 + cardName + " for 1 selected chip.\n");
		}
		else{
			//TAKE POINTS
			p.setQual(-2);
			//DISCARD CARD
			c.displayCardChooser();
			//PRINT FAIL
			Board.currentPlayText.append(p.getPlayerName() + " failed to play " 
					 + cardName + " and lost 2 Quality Points.\n" +
					 "Player must discard a card.\n");
		}
	}
	
	public String getName() {
		return ("Professor Englert");
	}
	
	public ImageInputStream displayPicture() {
		try {
			InputStream cardImage = new FileInputStream("C:/Users/Randy/Desktop/Cards/Card28.png");
			cardInput = ImageIO.createImageInputStream(cardImage);
		}
		catch (IOException e){
			System.out.println("I ugly");
		}
		return cardInput;
	}
}