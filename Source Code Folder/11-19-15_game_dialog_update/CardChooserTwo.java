import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class CardChooserTwo {
	JDialog j = new JDialog();
	JLabel image= new JLabel();
	JPanel cardPanel = new JPanel();
	JButton discardButton;
	int imageCounter = 0;
	
	
	
	public CardChooserTwo(final Player p) throws IOException {
		image = new JLabel(new ImageIcon(ImageIO.read(p.hand.get(0).displayPicture())));
		discardButton = new JButton("Discard");
		cardPanel.add(image);
		cardPanel.add(discardButton);
		discardButton.setLocation(10, 10);
		
		//mFrame.add(discardButton);
		j.add(cardPanel);
		
	
		
		p.setCurrentCardHand(p.hand.get(0));
		image.addMouseListener(new MouseAdapter(){  
			public void mouseClicked(MouseEvent e){ 	   	
				try {  
					do{
						imageCounter++;
						if(imageCounter>p.hand.size()-1) {
							imageCounter = 0;
							image.setIcon(new ImageIcon(ImageIO.read(p.hand.get(imageCounter).displayPicture())));
							p.setCurrentCardHand(p.hand.get(imageCounter));
						}
						else{
							image.setIcon(new ImageIcon(ImageIO.read(p.hand.get(imageCounter).displayPicture())));
							p.setCurrentCardHand(p.hand.get(imageCounter));
						}
					}while(p.hand.equals(null));
			
					}
				catch (IOException e1) {
					e1.printStackTrace();
				}
				
				
	      }  
	  }); 
		
		
		

		discardButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				
				  if(p.hand.get(0).getName() == p.hand.get(imageCounter).getName() ) {
              	p.discardCard(0);
              }
              else  if(p.hand.get(1).getName() == p.hand.get(imageCounter).getName() ) {
              	p.discardCard(1);
              }
              else  if(p.hand.get(2).getName() == p.hand.get(imageCounter).getName() ) {
              	p.discardCard(2);
              }
              else  if(p.hand.get(3).getName() == p.hand.get(imageCounter).getName() ) {
              	p.discardCard(3);
              }
              else  if(p.hand.get(4).getName() == p.hand.get(imageCounter).getName() ) {
              	p.discardCard(4);
              }
              else  if(p.hand.get(5).getName() == p.hand.get(imageCounter).getName() ) {
                	p.discardCard(5);
                }
              else  if(p.hand.get(6).getName() == p.hand.get(imageCounter).getName() ) {
                	p.discardCard(6);
                }
				 
				  j.dispose();
			}
			});
	}
		
	
	public void displayCardChooser() {
		j.setBounds (200,0,230,340);
		j.add(discardButton, BorderLayout.SOUTH);
		j.add(cardPanel, BorderLayout.CENTER);
		j.setModal(true);
		j.setVisible(true);
	}
}
